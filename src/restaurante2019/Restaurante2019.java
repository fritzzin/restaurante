package restaurante2019;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import java.sql.Connection;
import javax.swing.JOptionPane;
import tela.login.DlgLogin;

public class Restaurante2019 {

    public static Connection conexao = null;

    public static void main(String[] args) {
        if (ConexaoBD.getInstance().getConnection() != null) {
            DlgLogin login = new DlgLogin(null, true);

            login.setLocationRelativeTo(null);
            login.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Deu problema!");
        }
    }
}
