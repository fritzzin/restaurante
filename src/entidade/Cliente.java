package entidade;

/*
 * @author fritzzin
 */
public class Cliente {

    private int id;
    private String nome;
    private String telefone;
    private char situacao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public char getSituacao() {
        return situacao;
    }

    public void setSituacao(char situacao) {
        this.situacao = situacao;
    }

    @Override
    public String toString() {
        return "ID:          " + this.getId()
                + "\nNome:        " + this.getNome()
                + "\nTelefone:    " + this.getTelefone()
                + "\nSituacao:    " + this.getSituacao();
    }

}
