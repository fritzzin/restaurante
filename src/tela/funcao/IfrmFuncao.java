package tela.funcao;

/*
 * @author fritzzin
 */
import dao.FuncaoDAO;
import entidade.Funcao;
import javax.swing.JOptionPane;

public class IfrmFuncao extends javax.swing.JInternalFrame {

    int codigo = 0;

    public IfrmFuncao() {
        initComponents();
        this.setTitle("Funções");

        atualizarTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtCriterio = new javax.swing.JTextField();
        cmbSituacao = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_fornecedor = new javax.swing.JTable();
        btnFechar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnAtivarInativar = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(700, 430));

        jLabel1.setText("Critério:");

        txtCriterio.setMargin(new java.awt.Insets(0, 5, 0, 0));
        txtCriterio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCriterioKeyReleased(evt);
            }
        });

        cmbSituacao.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo"}));
        cmbSituacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSituacaoItemStateChanged(evt);
            }
        });

        jLabel2.setText("Situação");

        tbl_fornecedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_fornecedor);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnAtivarInativar.setText("Inativar");
        btnAtivarInativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarInativarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnNovo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtivarInativar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFechar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnNovo)
                    .addComponent(btnEditar)
                    .addComponent(btnAtivarInativar))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void txtCriterioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCriterioKeyReleased
        if (txtCriterio.getText().length() > 2) {
            atualizarTabela();
        } else if (txtCriterio.getText().length() == 0) {
            atualizarTabela();
        }
    }//GEN-LAST:event_txtCriterioKeyReleased

    private void cmbSituacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSituacaoItemStateChanged
        if (cmbSituacao.getSelectedItem().equals("Ativo")) {
            btnAtivarInativar.setText("Inativar");
        } else {
            btnAtivarInativar.setText("Ativar");
        }
        atualizarTabela();
    }//GEN-LAST:event_cmbSituacaoItemStateChanged

    private void btnAtivarInativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarInativarActionPerformed
        String idString = String.valueOf(tbl_fornecedor.getValueAt(tbl_fornecedor.getSelectedRow(), 0));
        int id = Integer.parseInt(idString);

        if (cmbSituacao.getSelectedItem().equals("Ativo")) {
            new FuncaoDAO().excluir(id);
        } else {
            new FuncaoDAO().ativar(id);
        }

        atualizarTabela();

    }//GEN-LAST:event_btnAtivarInativarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        String idString = String.valueOf(tbl_fornecedor.getValueAt(tbl_fornecedor.getSelectedRow(), 0));
        int id = Integer.parseInt(idString);

        Funcao f = new FuncaoDAO().consultarId(id);

        if (f == null) {
            JOptionPane.showMessageDialog(null, "Registro nao localizado!");
        } else {
            codigo = f.getId();
            new DlgFuncao(null, true, f, this).setVisible(true);
        }

        codigo = 0;
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        new DlgFuncao(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnNovoActionPerformed

    public void atualizarTabela() {
        String text = txtCriterio.getText();
        char situacao;

        if (cmbSituacao.getSelectedItem() == "Ativo") {
            situacao = 'A';
        } else {
            situacao = 'I';
        }

        new FuncaoDAO().popularTabela(tbl_fornecedor, text, situacao);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtivarInativar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_fornecedor;
    private javax.swing.JTextField txtCriterio;
    // End of variables declaration//GEN-END:variables
}
