package tela.produto;

import apoio.CombosDAO;
import apoio.Validacao;
import dao.ProdutoDAO;
import entidade.Produto;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * @author fritzzin
 */
public class DlgProduto extends javax.swing.JDialog {

    IfrmProduto janelaPai;
    Validacao validacao;
    Produto produto;

    int codigo = 0;

    public DlgProduto(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Cadastro Produto");
    }

    public DlgProduto(java.awt.Frame parent, boolean modal, IfrmProduto janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Cadastro Produto");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();

        cmbFornecedores.removeAllItems();
        new CombosDAO().popularCombo("fornecedor", cmbFornecedores);
    }

    public DlgProduto(java.awt.Frame parent, boolean modal, Produto p, IfrmProduto janelaPai) {
        super(parent, modal);
        initComponents();

        this.setTitle("Editar Produto");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();
        this.codigo = p.getId();

        int valido;

        txtNome.setText(p.getNome());
        txtPreco.setText(String.valueOf(p.getPreco_uni()));
        txtDescricao.setText(p.getDescricao());

        if (p.getSituacao() == 'A') {
            valido = 0;
        } else {
            valido = 1;
        }
        cmbTipoProduto.setSelectedIndex(valido);

        cmbTipoProduto.setSelectedItem(p.getTipo_produto());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        cmbTipoProduto = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtPreco = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new javax.swing.JTextArea();
        btnFechar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbSituacao = new javax.swing.JComboBox<>();
        cmbFornecedores = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Nome*:");

        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNomeKeyReleased(evt);
            }
        });

        cmbTipoProduto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SELECIONE", "COMIDA", "BEBIDA"}));
        cmbTipoProduto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoProdutoItemStateChanged(evt);
            }
        });

        jLabel2.setText("Tipo*:");

        jLabel3.setText("Preço*:");

        txtPreco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPrecoKeyReleased(evt);
            }
        });

        jLabel4.setText("Descrição:");

        txtDescricao.setColumns(20);
        txtDescricao.setRows(5);
        jScrollPane1.setViewportView(txtDescricao);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel5.setText("* = Campo obrigatorio");

        jLabel6.setText("Situação*:");

        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo"}));

        cmbFornecedores.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbFornecedores.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFornecedoresItemStateChanged(evt);
            }
        });

        jLabel7.setText("Fornecedor*:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addGap(43, 43, 43)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(txtPreco)
                            .addComponent(cmbSituacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbTipoProduto, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbFornecedores, 0, 218, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFornecedores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnSalvar)
                    .addComponent(jLabel5))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        produto = new Produto();

        String nome = txtNome.getText();
        int fornecedorId = cmbFornecedores.getSelectedIndex();
        String valorTxt = txtPreco.getText();
        Double valor;
        String descricao = txtDescricao.getText();
        String tipo = String.valueOf(cmbTipoProduto.getSelectedItem());

        // Validacoes
        boolean validNomeVazio = validacao.vazio(nome);
        boolean validValor = validacao.vazio(valorTxt);
        boolean validTipo = cmbTipoProduto.getSelectedItem() == "SELECIONE";
        boolean validFornecedor = cmbFornecedores.getSelectedIndex() == 0;

        if (!validNomeVazio && !validValor && !validTipo && !validFornecedor) {

            valor = Double.parseDouble(valorTxt);

            produto.setFornecedor_id(fornecedorId);
            produto.setNome(nome);
            produto.setPreco_uni(valor);
            produto.setDescricao(descricao);
            produto.setTipo_produto(tipo);

            if (cmbSituacao.getSelectedItem() == "Ativo") {
                produto.setSituacao('A');
            } else {
                produto.setSituacao('I');
            }

            ProdutoDAO dao = new ProdutoDAO();
            String retorno = null;

            if (codigo == 0) {
                retorno = dao.salvar(produto);
            } else {
                produto.setId(codigo);
                retorno = dao.atualizar(produto);
            }

            if (retorno == null) {
                JOptionPane.showMessageDialog(null, "Registro salvo com sucesso!");

                //Limpar campos
                txtNome.setText("");
                txtPreco.setText("");
                txtDescricao.setText("");

                txtNome.setBackground(Color.WHITE);
                txtPreco.setBackground(Color.WHITE);
                txtDescricao.setBackground(Color.WHITE);

                txtNome.requestFocus();

                janelaPai.atualizarTabela();

                cmbSituacao.setSelectedIndex(0);
                cmbTipoProduto.setSelectedIndex(0);

                if (codigo != 0) {
                    janelaPai.atualizarTabela();
                    this.dispose();
                }

                this.codigo = 0;
            } else {
                JOptionPane.showMessageDialog(null, "Ops! Problema ao salvar registro.");
            }

        } else {
            String erro = "";

            if (validNomeVazio) {
                erro += "Campo NOME inválido! O campo não pode estar VAZIO!!\n";
                txtNome.setBackground(Color.PINK);
            } else {
                txtNome.setBackground(Color.WHITE);
            }

            if (validValor) {
                erro += "Campo PRECO inválido! O campo não pode estar VAZIO!!\n";
                txtPreco.setBackground(Color.PINK);
            } else {
                txtPreco.setBackground(Color.WHITE);
            }

            if (validTipo) {
                erro += "Campo TIPO inválido! Selecione um tipo de produto!";
                cmbTipoProduto.setBackground(Color.PINK);
            } else {
                cmbTipoProduto.setBackground(Color.WHITE);
            }

            if (validFornecedor) {
                erro += "Campo FORNECEDOR inválido! Selecione um fornecedor!";
                cmbFornecedores.setBackground(Color.PINK);
            } else {
                cmbFornecedores.setBackground(Color.WHITE);
            }

            JOptionPane.showMessageDialog(null, erro);
        }

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyReleased
        String nome = txtNome.getText();

        boolean validNomeVazio = validacao.vazio(nome);

        if (validNomeVazio) {
            txtNome.setBackground(Color.PINK);

        } else {
            txtNome.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txtNomeKeyReleased

    private void txtPrecoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecoKeyReleased
        validarNumeros(txtPreco);
        String preco = txtPreco.getText();

        boolean validPrecoVazio = validacao.vazio(preco);

        if (validPrecoVazio) {
            txtPreco.setBackground(Color.PINK);
        } else {
            txtPreco.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txtPrecoKeyReleased

    private void cmbTipoProdutoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoProdutoItemStateChanged
        boolean validTipo = cmbTipoProduto.getSelectedItem() == "SELECIONE";

        if (validTipo) {
            cmbTipoProduto.setBackground(Color.PINK);
        } else {
            cmbTipoProduto.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_cmbTipoProdutoItemStateChanged

    private void cmbFornecedoresItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFornecedoresItemStateChanged
        boolean validFornecedor = cmbFornecedores.getSelectedIndex() == 0;

        if (validFornecedor) {
            cmbFornecedores.setBackground(Color.PINK);
        } else {
            cmbFornecedores.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_cmbFornecedoresItemStateChanged

    public String validarNumeros(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temLetras(texto) || texto.matches(".*[,]+.*")) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgProduto dialog = new DlgProduto(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cmbFornecedores;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JComboBox<String> cmbTipoProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtDescricao;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPreco;
    // End of variables declaration//GEN-END:variables
}
