package tela.produto;

/*
 * @author fritzzin
 */
import dao.ProdutoDAO;
import entidade.Produto;
import javax.swing.JOptionPane;

public class IfrmProduto extends javax.swing.JInternalFrame {

    int codigo = 0;

    public IfrmProduto() {
        initComponents();
        this.setTitle("Produtos");

        atualizarTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblProduto = new javax.swing.JTable();
        txtCriterio = new javax.swing.JTextField();
        lbl_criterio = new javax.swing.JLabel();
        btnFechar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbSituacao = new javax.swing.JComboBox<>();
        btnAtivarInativar = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(700, 430));

        tblProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblProduto);

        txtCriterio.setMargin(new java.awt.Insets(0, 5, 0, 0));
        txtCriterio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCriterioKeyReleased(evt);
            }
        });

        lbl_criterio.setText("Critério:");

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jLabel1.setText("Situação");

        cmbSituacao.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo"}));
        cmbSituacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSituacaoItemStateChanged(evt);
            }
        });

        btnAtivarInativar.setText("Inativar");
        btnAtivarInativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivarInativarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_criterio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNovo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtivarInativar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFechar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_criterio)
                    .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnEditar)
                    .addComponent(btnNovo)
                    .addComponent(btnAtivarInativar))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        new DlgProduto(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        String idString = String.valueOf(tblProduto.getValueAt(tblProduto.getSelectedRow(), 0));
        int id = Integer.parseInt(idString);

        Produto p = new ProdutoDAO().consultarId(id);
        if (p == null) {
            JOptionPane.showMessageDialog(null, "Registro nao localizado!");
        } else {
            codigo = p.getId();
            new DlgProduto(null, true, p, this).setVisible(true);
        }

        codigo = 0;
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnAtivarInativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivarInativarActionPerformed
        String idString = String.valueOf(tblProduto.getValueAt(tblProduto.getSelectedRow(), 0));
        int id = Integer.parseInt(idString);

        if (cmbSituacao.getSelectedItem().equals("Ativo")) {
            new ProdutoDAO().excluir(id);
        } else {
            new ProdutoDAO().ativar(id);
        }

        atualizarTabela();
    }//GEN-LAST:event_btnAtivarInativarActionPerformed

    private void cmbSituacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSituacaoItemStateChanged
        if (cmbSituacao.getSelectedItem().equals("Ativo")) {
            btnAtivarInativar.setText("Inativar");
        } else {
            btnAtivarInativar.setText("Ativar");
        }
        atualizarTabela();
    }//GEN-LAST:event_cmbSituacaoItemStateChanged

    private void txtCriterioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCriterioKeyReleased
        if (txtCriterio.getText().length() > 2) {
            atualizarTabela();
        } else if (txtCriterio.getText().length() == 0) {
            atualizarTabela();
        }
    }//GEN-LAST:event_txtCriterioKeyReleased

    public void atualizarTabela() {
        String text = txtCriterio.getText();
        char situacao;

        if (cmbSituacao.getSelectedItem() == "Ativo") {
            situacao = 'A';
        } else {
            situacao = 'I';
        }

        new ProdutoDAO().popularTabela(tblProduto, text, situacao);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtivarInativar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_criterio;
    private javax.swing.JTable tblProduto;
    private javax.swing.JTextField txtCriterio;
    // End of variables declaration//GEN-END:variables
}
