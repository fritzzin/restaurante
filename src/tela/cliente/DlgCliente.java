package tela.cliente;

/*
 * @author fritzzin
 */
import apoio.Formatacao;
import apoio.Validacao;
import dao.ClienteDAO;
import entidade.Cliente;
import java.awt.Color;
import javax.swing.JOptionPane;

public class DlgCliente extends javax.swing.JDialog {

    IfrmCliente janelaPai;
    Validacao validacao;
    Cliente cliente;

    int codigo = 0;

    public DlgCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Cadastro Cliente");
        this.validacao = new Validacao();

        //Formatar campos
        Formatacao.formatarTelefone(ftxtTelefone);
    }

    public DlgCliente(java.awt.Frame parent, boolean modal, IfrmCliente janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Cadastro Cliente");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();

        //Formatar campos
        Formatacao.formatarTelefone(ftxtTelefone);
    }

    public DlgCliente(java.awt.Frame parent, boolean modal, Cliente c, IfrmCliente janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        //Formatar campos
        Formatacao.formatarTelefone(ftxtTelefone);

        this.setTitle("Editando Cliente");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();
        this.codigo = c.getId();

        txtNome.setText(c.getNome());
        ftxtTelefone.setText(c.getTelefone());

        int valido;

        if (c.getSituacao() == 'A') {
            valido = 0;
        } else {
            valido = 1;
        }
        cmbSituacao.setSelectedIndex(valido);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbSituacao = new javax.swing.JComboBox<>();
        btnCancelar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        ftxtTelefone = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Nome*: ");

        txtNome.setMargin(new java.awt.Insets(0, 5, 0, 0));
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNomeKeyReleased(evt);
            }
        });

        jLabel2.setText("Telefone*:");

        jLabel3.setText("Situação*:");

        cmbSituacao.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ativo", "Inativo"}));

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        ftxtTelefone.setMargin(new java.awt.Insets(0, 5, 0, 0));

        jLabel4.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel4.setText("* = Campo obrigatorio");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ftxtTelefone)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 193, Short.MAX_VALUE))
                            .addComponent(txtNome)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ftxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar)
                    .addComponent(jLabel4))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        janelaPai.atualizarTabela();
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        cliente = new Cliente();

        String nome = txtNome.getText();
        String telefone = ftxtTelefone.getText();

        // Validacoes
        boolean validNomeNumeros = validacao.temNumeros(nome);
        boolean validNomeVazio = validacao.vazio(nome);
        boolean validTelefone = validacao.validarTelefoneBoolean(ftxtTelefone);

        if (!validNomeNumeros && !validNomeVazio && validTelefone) {
            // setters
            cliente.setNome(nome);
            cliente.setTelefone(telefone);

            if (cmbSituacao.getSelectedItem() == "Ativo") {
                cliente.setSituacao('A');
            } else {
                cliente.setSituacao('I');
            }

            ClienteDAO dao = new ClienteDAO();
            String retorno = null;

            if (codigo == 0) {
                retorno = dao.salvar(cliente);
            } else {
                cliente.setId(codigo);
                retorno = dao.atualizar(cliente);
            }

            if (retorno == null) {
                JOptionPane.showMessageDialog(null, "Registro salvo com sucesso!");

                //Limpar campos
                txtNome.setText("");
                ftxtTelefone.setText("");
                cmbSituacao.setSelectedIndex(0);

                //Remover autocomplete
                Formatacao.formatarTelefone(ftxtTelefone);

                //Remover background
                txtNome.setBackground(Color.WHITE);
                ftxtTelefone.setBackground(Color.WHITE);

                //Posicionar cursor
                txtNome.requestFocus();

                // Atualizar tabela
                janelaPai.atualizarTabela();

                // Caso estiver editando fechar o dlg
                if (codigo != 0) {
                    janelaPai.atualizarTabela();
                    this.dispose();
                }

                this.codigo = 0;
            } else {
                JOptionPane.showMessageDialog(null, "Ops! Problemas ao salvar registro.");
            }

        } else {
            // Mostrar Dlg com uma mensagem de erro
            String erro = "";

            // Campo NOME
            if (validNomeNumeros) {
                erro += "Campo NOME inválido! O campo pode possuir apenas LETRAS!\n";
            }

            if (validNomeVazio) {
                erro += "Campo NOME inválido! O campo não pode estar VAZIO!!\n";
            }

            if (validNomeNumeros || validNomeVazio) {
                txtNome.setBackground(Color.PINK);
            } else {
                txtNome.setBackground(Color.WHITE);
            }

            // Campo TELEFONE
            if (!validTelefone) {
                erro += "Campo TELEFONE inválido! Preencher com um telefone válido!";
                ftxtTelefone.setBackground(Color.PINK);
            } else {
                ftxtTelefone.setBackground(Color.WHITE);
            }

            JOptionPane.showMessageDialog(null, erro);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyReleased
        String nome = txtNome.getText();

        boolean validNomeNumeros = validacao.temNumeros(nome);
        boolean validNomeVazio = validacao.vazio(nome);

        if (validNomeNumeros || validNomeVazio) {
            txtNome.setBackground(Color.PINK);
        } else {
            txtNome.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txtNomeKeyReleased

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgCliente dialog = new DlgCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JFormattedTextField ftxtTelefone;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables
}
