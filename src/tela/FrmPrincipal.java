package tela;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import entidade.Funcionario;
import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import reports.comanda.DlgComandaData;
import reports.comanda.DlgComandaSituacao;
import tela.cliente.IfrmCliente;
import tela.comanda.DlgComanda;
import tela.comanda.IfrmComanda;
import tela.contaPagar.IfrmContaPagar;
import tela.fornecedor.IfrmFornecedor;
import tela.funcao.IfrmFuncao;
import tela.funcionario.IfrmFuncionario;
import tela.produto.IfrmProduto;
import tela.sobre.DlgSobre;

public class FrmPrincipal extends javax.swing.JFrame {

    Funcionario funcionario;

    public FrmPrincipal() {
        initComponents();
        this.setTitle("Restaurante BisTECH");
        this.setExtendedState(MAXIMIZED_BOTH);
    }

    public FrmPrincipal(Funcionario funcionario) {
        initComponents();
        this.setTitle("Restaurante BisTECH");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.funcionario = funcionario;

        System.out.println(funcionario.getNome());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ImageIcon icon = new ImageIcon(getClass().getResource("/imagem/background.jpg"));
        Image image = icon.getImage();
        jDesktopPane1 = new javax.swing.JDesktopPane(){

            public void paintComponent(Graphics g){
                g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
            }

        };
        btnNovaComanda = new javax.swing.JButton();
        btnClientes = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu_cadastro = new javax.swing.JMenu();
        itmCliente = new javax.swing.JMenuItem();
        itmFornecedor = new javax.swing.JMenuItem();
        itmFuncionario = new javax.swing.JMenuItem();
        itmFuncao = new javax.swing.JMenuItem();
        itmProduto = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        itmComanda = new javax.swing.JMenuItem();
        itmHistoricoComanda = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        itmHistoricoConta = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        itmListaCliente = new javax.swing.JMenuItem();
        itmListaFornecedor = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        itmRelatorioComandaSituacao = new javax.swing.JMenuItem();
        itmRelatorioComandaData = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        itmSobre = new javax.swing.JMenuItem();
        itmSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnNovaComanda.setText("Nova Comanda");
        btnNovaComanda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaComandaActionPerformed(evt);
            }
        });

        btnClientes.setText("Clientes");
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        jDesktopPane1.setLayer(btnNovaComanda, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jDesktopPane1.setLayer(btnClientes, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnNovaComanda, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(btnClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(424, Short.MAX_VALUE))
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNovaComanda, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(323, Short.MAX_VALUE))
        );

        menu_cadastro.setText("Cadastro");

        itmCliente.setText("Clientes");
        itmCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmClienteActionPerformed(evt);
            }
        });
        menu_cadastro.add(itmCliente);

        itmFornecedor.setText("Fornecedores");
        itmFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmFornecedorActionPerformed(evt);
            }
        });
        menu_cadastro.add(itmFornecedor);

        itmFuncionario.setText("Funcionários");
        itmFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmFuncionarioActionPerformed(evt);
            }
        });
        menu_cadastro.add(itmFuncionario);

        itmFuncao.setText("Funções");
        itmFuncao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmFuncaoActionPerformed(evt);
            }
        });
        menu_cadastro.add(itmFuncao);

        itmProduto.setText("Produtos");
        itmProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmProdutoActionPerformed(evt);
            }
        });
        menu_cadastro.add(itmProduto);

        jMenuBar1.add(menu_cadastro);

        jMenu2.setText("Comanda");

        itmComanda.setText("Nova comanda");
        itmComanda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmComandaActionPerformed(evt);
            }
        });
        jMenu2.add(itmComanda);

        itmHistoricoComanda.setText("Histórico de comandas");
        itmHistoricoComanda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmHistoricoComandaActionPerformed(evt);
            }
        });
        jMenu2.add(itmHistoricoComanda);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Contas");

        itmHistoricoConta.setText("Histórico de contas");
        itmHistoricoConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmHistoricoContaActionPerformed(evt);
            }
        });
        jMenu3.add(itmHistoricoConta);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Relatórios");

        jMenu5.setText("Listas");

        itmListaCliente.setText("Clientes");
        itmListaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmListaClienteActionPerformed(evt);
            }
        });
        jMenu5.add(itmListaCliente);

        itmListaFornecedor.setText("Fornecedores");
        itmListaFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmListaFornecedorActionPerformed(evt);
            }
        });
        jMenu5.add(itmListaFornecedor);

        jMenu4.add(jMenu5);

        jMenu6.setText("Comandas");

        itmRelatorioComandaSituacao.setText("Comanda por situação");
        itmRelatorioComandaSituacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmRelatorioComandaSituacaoActionPerformed(evt);
            }
        });
        jMenu6.add(itmRelatorioComandaSituacao);

        itmRelatorioComandaData.setText("Comanda entre Datas");
        itmRelatorioComandaData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmRelatorioComandaDataActionPerformed(evt);
            }
        });
        jMenu6.add(itmRelatorioComandaData);

        jMenu4.add(jMenu6);

        jMenuBar1.add(jMenu4);

        jMenu1.setText("Ajuda");

        itmSobre.setText("Sobre");
        itmSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmSobreActionPerformed(evt);
            }
        });
        jMenu1.add(itmSobre);

        itmSair.setText("Sair");
        itmSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itmSairActionPerformed(evt);
            }
        });
        jMenu1.add(itmSair);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jDesktopPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void itmClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmClienteActionPerformed
        if (!verificarJanela("Clientes")) {
            IfrmCliente cliente = new IfrmCliente();
            jDesktopPane1.add(cliente);
            cliente.setVisible(true);
        }
    }//GEN-LAST:event_itmClienteActionPerformed

    private void itmFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmFornecedorActionPerformed
        if (!verificarJanela("Fornecedores")) {
            IfrmFornecedor fornecedor = new IfrmFornecedor();
            jDesktopPane1.add(fornecedor);
            fornecedor.setVisible(true);
        }
    }//GEN-LAST:event_itmFornecedorActionPerformed

    private void itmSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_itmSairActionPerformed

    private void itmFuncaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmFuncaoActionPerformed
        if (!verificarJanela("Funções")) {
            IfrmFuncao funcao = new IfrmFuncao();
            jDesktopPane1.add(funcao);
            funcao.setVisible(true);
        }
    }//GEN-LAST:event_itmFuncaoActionPerformed

    private void itmFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmFuncionarioActionPerformed
        if (!verificarJanela("Funcionários")) {
            IfrmFuncionario funcionario = new IfrmFuncionario();
            jDesktopPane1.add(funcionario);
            funcionario.setVisible(true);
        }
    }//GEN-LAST:event_itmFuncionarioActionPerformed

    private void itmComandaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmComandaActionPerformed
        if (!verificarJanela("Nova Comanda")) {
            new DlgComanda(this, rootPaneCheckingEnabled, funcionario.getId()).setVisible(true);
        }
    }//GEN-LAST:event_itmComandaActionPerformed

    private void itmHistoricoComandaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmHistoricoComandaActionPerformed
        if (!verificarJanela("Comandas")) {
            IfrmComanda comanda = new IfrmComanda(funcionario.getId());
            jDesktopPane1.add(comanda);
            comanda.setVisible(true);
        }
    }//GEN-LAST:event_itmHistoricoComandaActionPerformed

    private void itmListaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmListaClienteActionPerformed
        try {
            // Compila o relatorio
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/reports/clientes/clientes.jrxml"));

            // Mapeia campos de parametros para o relatorio, mesmo que nao existam
            Map parametros = new HashMap();

            // Executa relatoio
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

            // Exibe resultado em video
            JasperViewer.viewReport(impressao, false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }//GEN-LAST:event_itmListaClienteActionPerformed

    private void itmListaFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmListaFornecedorActionPerformed
        try {
            JasperReport relatorio = JasperCompileManager.compileReport((getClass().getResourceAsStream("/reports/fornecedores/fornecedores.jrxml")));

            Map parametros = new HashMap();

            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

            JasperViewer.viewReport(impressao, false);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "Erro ago gerar relatório: " + e);
        }
    }//GEN-LAST:event_itmListaFornecedorActionPerformed

    private void itmRelatorioComandaSituacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmRelatorioComandaSituacaoActionPerformed
        new DlgComandaSituacao(this, true).setVisible(true);
    }//GEN-LAST:event_itmRelatorioComandaSituacaoActionPerformed

    private void itmRelatorioComandaDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmRelatorioComandaDataActionPerformed
        new DlgComandaData(this, true).setVisible(true);
    }//GEN-LAST:event_itmRelatorioComandaDataActionPerformed

    private void itmProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmProdutoActionPerformed
        if (!verificarJanela("Produtos")) {
            IfrmProduto produto = new IfrmProduto();
            jDesktopPane1.add(produto);
            produto.setVisible(true);
        }
    }//GEN-LAST:event_itmProdutoActionPerformed

    private void itmHistoricoContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmHistoricoContaActionPerformed
        if (!verificarJanela("Contas a pagar")) {
            IfrmContaPagar contaPagar = new IfrmContaPagar();
            jDesktopPane1.add(contaPagar);
            contaPagar.setVisible(true);
        }
    }//GEN-LAST:event_itmHistoricoContaActionPerformed

    private void btnNovaComandaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaComandaActionPerformed
        if (!verificarJanela("Nova Comanda")) {
            new DlgComanda(this, rootPaneCheckingEnabled, funcionario.getId()).setVisible(true);
        }
    }//GEN-LAST:event_btnNovaComandaActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        if (!verificarJanela("Clientes")) {
            IfrmCliente cliente = new IfrmCliente();
            jDesktopPane1.add(cliente);
            cliente.setVisible(true);
        }
    }//GEN-LAST:event_btnClientesActionPerformed

    private void itmSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itmSobreActionPerformed
        if (!verificarJanela("Sobre")) {
            new DlgSobre(null, true).setVisible(true);
        }
    }//GEN-LAST:event_itmSobreActionPerformed

    public boolean verificarJanela(String titulo) {
        JInternalFrame[] abertas = jDesktopPane1.getAllFrames();

        for (JInternalFrame aberta : abertas) {
            if (aberta.getTitle().equals(titulo)) {
                System.out.println("Já existe uma janela \"" + titulo + "\" aberta!");
                return true;
            }
        }
        return false;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClientes;
    private javax.swing.JButton btnNovaComanda;
    private javax.swing.JMenuItem itmCliente;
    private javax.swing.JMenuItem itmComanda;
    private javax.swing.JMenuItem itmFornecedor;
    private javax.swing.JMenuItem itmFuncao;
    private javax.swing.JMenuItem itmFuncionario;
    private javax.swing.JMenuItem itmHistoricoComanda;
    private javax.swing.JMenuItem itmHistoricoConta;
    private javax.swing.JMenuItem itmListaCliente;
    private javax.swing.JMenuItem itmListaFornecedor;
    private javax.swing.JMenuItem itmProduto;
    private javax.swing.JMenuItem itmRelatorioComandaData;
    private javax.swing.JMenuItem itmRelatorioComandaSituacao;
    private javax.swing.JMenuItem itmSair;
    private javax.swing.JMenuItem itmSobre;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu menu_cadastro;
    // End of variables declaration//GEN-END:variables
}
