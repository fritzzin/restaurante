package tela.contaPagar;

import apoio.Formatacao;
import apoio.Validacao;
import dao.ContaPagarDAO;
import dao.FuncionarioDAO;
import entidade.ContaPagar;
import entidade.Funcionario;
import java.awt.Color;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * @author fritzzin
 */
public class DlgContaPagar extends javax.swing.JDialog {

    IfrmContaPagar janelaPai;
    Validacao validacao;
    ContaPagar contaPagar;

    int codigo = 0;

    public DlgContaPagar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public DlgContaPagar(java.awt.Frame parent, boolean modal, IfrmContaPagar janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Nova Conta");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();

        Formatacao.formatarData(ftxtDataVencimento);
        Formatacao.formatarData(ftxtDataPagamento);
    }

    public DlgContaPagar(java.awt.Frame parent, boolean modal, IfrmContaPagar janelaPai, ContaPagar contaPagar) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Editando Conta");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();
        this.contaPagar = contaPagar;
        this.codigo = contaPagar.getId();

        Formatacao.formatarData(ftxtDataVencimento);
        Formatacao.formatarData(ftxtDataPagamento);

        String funcionarioId = String.valueOf(contaPagar.getFuncionarioId());
        String valor = String.valueOf(contaPagar.getValor());
        String dataVencimento = Formatacao.removerFormatacao(contaPagar.getDataVencimento());

        String dataPagamento = "";
        if (contaPagar.getDataPagamento() != null) {
            dataPagamento = Formatacao.removerFormatacao(contaPagar.getDataPagamento());
            cmbMetodoPagamento.setEnabled(true);
        }

        String descricao = contaPagar.getDescricao();

        // setters
        txtFuncionarioId.setText(funcionarioId);
        txtValor.setText(valor);
        ftxtDataVencimento.setText(dataVencimento);
        ftxtDataPagamento.setText(dataPagamento);
        txtDescricao.setText(descricao);

        switch (contaPagar.getTipoConta()) {
            case "LUZ":
                cmbTipoConta.setSelectedIndex(1);
                break;
            case "ÁGUA":
                cmbTipoConta.setSelectedIndex(2);
                break;
            case "GÁS":
                cmbTipoConta.setSelectedIndex(3);
                break;
            case "PRODUTOS":
                cmbTipoConta.setSelectedIndex(4);
                break;
        }

        if (cmbMetodoPagamento.isEnabled()) {
            switch (contaPagar.getMetodoPagamento()) {
                case "CRÉDITO":
                    cmbMetodoPagamento.setSelectedIndex(1);
                    break;
                case "DÉBITO":
                    cmbMetodoPagamento.setSelectedIndex(2);
                    break;
                case "DINHEIRO":
                    cmbMetodoPagamento.setSelectedIndex(3);
                    break;
            }
        }

        Funcionario f = new FuncionarioDAO().consultarId(contaPagar.getFuncionarioId());
        setParametrosFuncionario(f);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtFuncionarioId = new javax.swing.JTextField();
        txtFuncionarioNome = new javax.swing.JTextField();
        btnListarFuncionario = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        txtValor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        ftxtDataVencimento = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        ftxtDataPagamento = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbTipoConta = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        cmbMetodoPagamento = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Funcionário*:");

        txtFuncionarioId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtFuncionarioIdFocusLost(evt);
            }
        });
        txtFuncionarioId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFuncionarioIdKeyReleased(evt);
            }
        });

        txtFuncionarioNome.setEditable(false);

        btnListarFuncionario.setText("Listar");
        btnListarFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListarFuncionarioActionPerformed(evt);
            }
        });

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        txtValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtValorKeyReleased(evt);
            }
        });

        jLabel2.setText("Valor*:");

        jLabel3.setText("Data Vencimento*:");

        ftxtDataPagamento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ftxtDataPagamentoFocusLost(evt);
            }
        });

        jLabel4.setText("Data Pagamento:");

        cmbTipoConta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SELECIONE", "LUZ", "ÁGUA", "GÁS", "PRODUTOS"}));

        jLabel5.setText("Tipo de conta*:");

        cmbMetodoPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SELECIONE", "CRÉDITO", "DÉBITO", "DINHEIRO" }));
        cmbMetodoPagamento.setEnabled(false);

        jLabel6.setText("Forma de Pagamento*:");

        txtDescricao.setColumns(20);
        txtDescricao.setRows(5);
        jScrollPane1.setViewportView(txtDescricao);

        jLabel7.setText("Descrição:");

        jLabel8.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel8.setText("* = Campo obrigatorio");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(6, 6, 6))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel3)
                                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbMetodoPagamento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbTipoConta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ftxtDataPagamento)
                            .addComponent(ftxtDataVencimento)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtFuncionarioId, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFuncionarioNome, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnListarFuncionario))
                            .addComponent(txtValor)
                            .addComponent(jScrollPane1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtFuncionarioId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFuncionarioNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnListarFuncionario))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftxtDataVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftxtDataPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoConta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMetodoPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnFechar)
                            .addComponent(btnSalvar)
                            .addComponent(jLabel8)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtFuncionarioIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFuncionarioIdKeyReleased
        validarNumeros(txtFuncionarioId);
    }//GEN-LAST:event_txtFuncionarioIdKeyReleased

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        contaPagar = new ContaPagar();

        String idFuncionarioTexto = txtFuncionarioId.getText();
        boolean validIdFuncionarioVazio = Validacao.vazio(idFuncionarioTexto);
        int funcionarioId = 0;
        if (!validIdFuncionarioVazio) {
            funcionarioId = Integer.parseInt(idFuncionarioTexto);
        }

        String valorTexto = txtValor.getText();
        boolean validValorVazio = Validacao.vazio(valorTexto);
        double valor = 0.0;
        if (!validValorVazio) {
            valor = Double.parseDouble(valorTexto);
        }

        String dataVencimento = ftxtDataVencimento.getText();
        boolean validDataVencimento;
        if (Validacao.vazio(dataVencimento)) {
            validDataVencimento = false;
        } else {
            validDataVencimento = validarData(ftxtDataVencimento);
        }

        String dataPagamento = ftxtDataPagamento.getText();
        boolean validDataPagamento;
        boolean validDataPagamentoVazio = Validacao.vazio(dataPagamento);
        if (validDataPagamentoVazio) {
            validDataPagamento = true;
            dataPagamento = "";
        } else {
            validDataPagamento = validarData(ftxtDataPagamento);
        }

        String tipoConta = cmbTipoConta.getSelectedItem().toString();
        boolean validTipoConta = !tipoConta.equals("SELECIONE");

        String metodoPagamento = cmbMetodoPagamento.getSelectedItem().toString();
        boolean validMetodoPagamento = true;
        if (cmbMetodoPagamento.isEnabled()) {
            validMetodoPagamento = !metodoPagamento.equals("SELECIONE");
        } else {
            metodoPagamento = "NÃO DEFINIDO";
        }
        String descricao = txtDescricao.getText();

        if (!validValorVazio
                && !validIdFuncionarioVazio
                && validDataVencimento
                && validDataPagamento
                && validTipoConta
                && validMetodoPagamento) {

            contaPagar.setFuncionarioId(funcionarioId);
            contaPagar.setValor(valor);
            contaPagar.setDataVencimento(dataVencimento);
            contaPagar.setDataPagamento(dataPagamento);
            contaPagar.setTipoConta(tipoConta);
            contaPagar.setMetodoPagamento(metodoPagamento);
            contaPagar.setDescricao(descricao);

            if (Validacao.vazio(dataPagamento)) {
                contaPagar.setSituacao('I');
            } else {
                contaPagar.setSituacao('A');
            }

            System.out.println("CONTA A PAGAR: " + contaPagar.getSituacao());

            ContaPagarDAO dao = new ContaPagarDAO();
            String retorno = null;

            if (codigo == 0) {
                if (contaPagar.getSituacao() == 'I') {
                    retorno = dao.salvarSemData(contaPagar);
                } else {
                    retorno = dao.salvar(contaPagar);
                }
            } else {
                contaPagar.setId(codigo);

                retorno = dao.atualizar(contaPagar);

                if (contaPagar.getSituacao() == 'I') {
                    retorno = dao.removerDataPagamento(contaPagar);
                } else {
                    retorno = dao.atualizarDataPagamento(contaPagar);
                }
            }

            if (retorno == null) {
                JOptionPane.showMessageDialog(null, "Registro salvo com sucesso!");

                txtFuncionarioId.setText("");
                txtFuncionarioNome.setText("");
                ftxtDataVencimento.setText("");
                ftxtDataPagamento.setText("");
                txtValor.setText("");
                cmbTipoConta.setSelectedIndex(0);
                cmbMetodoPagamento.setSelectedIndex(0);
                txtDescricao.setText("");

                Formatacao.formatarData(ftxtDataVencimento);
                Formatacao.formatarData(ftxtDataPagamento);

                txtFuncionarioId.setBackground(Color.WHITE);
                txtFuncionarioNome.setBackground(Color.WHITE);
                ftxtDataVencimento.setBackground(Color.WHITE);
                ftxtDataPagamento.setBackground(Color.WHITE);
                txtValor.setBackground(Color.WHITE);
                cmbTipoConta.setBackground(Color.WHITE);
                cmbMetodoPagamento.setBackground(Color.WHITE);
                txtDescricao.setBackground(Color.WHITE);

                cmbMetodoPagamento.setEnabled(false);

                janelaPai.atualizarTabela();

                if (codigo != 0) {
                    this.dispose();
                }

                this.codigo = 0;
            } else {
                JOptionPane.showMessageDialog(null, "Ops! Problemas ao salvar registro.");
            }
        } else {
            String erro = "";

            if (validIdFuncionarioVazio) {
                erro += "Campo FUNCIONARIO inválido! Campo não pode estar vazio.";
                txtFuncionarioId.setBackground(Color.PINK);
                txtFuncionarioNome.setBackground(Color.PINK);
            } else {
                txtFuncionarioId.setBackground(Color.WHITE);
                txtFuncionarioNome.setBackground(Color.WHITE);
            }

            if (validValorVazio) {
                erro += "Campo VALOR inválido! Campo não pode estar vazio.";
                txtValor.setBackground(Color.PINK);
            } else {
                txtValor.setBackground(Color.WHITE);
            }

            if (!validDataVencimento) {
                erro += "Campo DATA VENCIMENTO inválido! Insira uma data válido.";
                ftxtDataVencimento.setBackground(Color.PINK);
            } else {
                ftxtDataVencimento.setBackground(Color.WHITE);
            }

            if (!validDataPagamento) {
                erro += "Campo DATA PAGAMENTO inválido! Insira uma data válida.";
                ftxtDataPagamento.setBackground(Color.PINK);
                Formatacao.formatarData(ftxtDataPagamento);
            } else {
                ftxtDataPagamento.setBackground(Color.WHITE);
            }

            if (!validTipoConta) {
                erro += "Campo TIPO DE CONTA inválido! Selecione um tipo válido.";
                cmbTipoConta.setBackground(Color.PINK);
            } else {
                cmbTipoConta.setBackground(Color.WHITE);
            }

            if (!validMetodoPagamento) {
                erro += " Campo FORMA DE PAGAMENTO inválido! Selecione um tipo válido.";
                cmbMetodoPagamento.setBackground(Color.PINK);
            } else {
                cmbMetodoPagamento.setBackground(Color.WHITE);
            }

            JOptionPane.showMessageDialog(this, erro);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnListarFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListarFuncionarioActionPerformed
        new DlgListarFuncionario(this, rootPaneCheckingEnabled, this).setVisible(true);
    }//GEN-LAST:event_btnListarFuncionarioActionPerformed

    private void txtValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorKeyReleased
        validarNumeros(txtValor);
    }//GEN-LAST:event_txtValorKeyReleased

    private void txtFuncionarioIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFuncionarioIdFocusLost
        String texto = txtFuncionarioId.getText().trim();
        if (!texto.equals("")) {
            int id = Integer.parseInt(texto);
            Funcionario f = new FuncionarioDAO().consultarId(id);

            if (f != null) {
                setParametrosFuncionario(f);
            } else {
                txtFuncionarioId.setText("");
                txtFuncionarioNome.setText("");

                txtFuncionarioId.setEditable(true);
                txtFuncionarioId.requestFocus();

                JOptionPane.showMessageDialog(this, "ID não existende");
            }
        }
    }//GEN-LAST:event_txtFuncionarioIdFocusLost

    private void ftxtDataPagamentoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ftxtDataPagamentoFocusLost
        String dataSemFormatacao = Formatacao.removerFormatacao(ftxtDataPagamento.getText());
        if (!Validacao.vazio(dataSemFormatacao)) {
            cmbMetodoPagamento.setEnabled(true);
        } else {
            cmbMetodoPagamento.setEnabled(false);
        }
    }//GEN-LAST:event_ftxtDataPagamentoFocusLost

    public String validarNumeros(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temLetras(texto) || texto.contains("-") || texto.contains(",")) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    public void setParametrosFuncionario(Funcionario f) {
        txtFuncionarioId.setText(String.valueOf(f.getId()));
        txtFuncionarioNome.setText(f.getNome());
    }

    public boolean validarData(JFormattedTextField textField) {
        boolean retorno;
        try {
            String data = textField.getText();
            boolean valido = Validacao.validarDataFormatada(data);

            if (!valido) {
                retorno = false;
            } else {
                retorno = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            retorno = false;
        }
        return retorno;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgContaPagar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgContaPagar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgContaPagar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgContaPagar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgContaPagar dialog = new DlgContaPagar(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnListarFuncionario;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cmbMetodoPagamento;
    private javax.swing.JComboBox<String> cmbTipoConta;
    private javax.swing.JFormattedTextField ftxtDataPagamento;
    private javax.swing.JFormattedTextField ftxtDataVencimento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtDescricao;
    private javax.swing.JTextField txtFuncionarioId;
    private javax.swing.JTextField txtFuncionarioNome;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
