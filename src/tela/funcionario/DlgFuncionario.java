package tela.funcionario;

/*
 * @author fritzzin
 */
import apoio.Encriptar;
import apoio.Formatacao;
import apoio.Validacao;
import dao.FuncaoDAO;
import dao.FuncionarioDAO;
import entidade.Funcao;
import entidade.Funcionario;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DlgFuncionario extends javax.swing.JDialog {

    IfrmFuncionario janelaPai;
    Validacao validacao;
    Funcionario funcionario;

    int codigo = 0;

    public DlgFuncionario(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Cadastro Funcionário");
    }

    public DlgFuncionario(java.awt.Frame parent, boolean modal, IfrmFuncionario janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Cadastro Funcionário");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();

        Formatacao.formatarData(ftxtDataAdmissao);
        Formatacao.formatarData(ftxtDataDemissao);
        Formatacao.formatarTelefone(ftxtTelefone);
    }

    public DlgFuncionario(java.awt.Frame parent, boolean modal, Funcionario f, IfrmFuncionario janelaPai) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Editando Funcionário");
        this.janelaPai = janelaPai;
        this.validacao = new Validacao();
        this.codigo = f.getId();

        Formatacao.formatarData(ftxtDataAdmissao);
        Formatacao.formatarData(ftxtDataDemissao);
        Formatacao.formatarTelefone(ftxtTelefone);

        String nome = f.getNome();
        String funcaoId = String.valueOf(f.getFuncao_id());
        String telefone = f.getTelefone();

        String dataAdmissao = Formatacao.removerFormatacao(f.getData_admissao());

        String dataDemissao = "";
        if (f.getData_demissao() != null) {
            dataDemissao = Formatacao.removerFormatacao(f.getData_demissao());
        } else {
            dataDemissao = "";
        }

        String salario = String.valueOf(f.getSalario());
        String usuario = f.getLogin();

        txtNome.setText(nome);
        txtFuncaoId.setText(funcaoId);
        ftxtTelefone.setText(telefone);
        ftxtDataAdmissao.setText(dataAdmissao);
        ftxtDataDemissao.setText(dataDemissao);
        txtSalario.setText(salario);
        txtUsuario.setText(usuario);

        txtUsuario.setEditable(false);

        txtFuncaoId.requestFocus();
        txtSalario.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        lblNome = new javax.swing.JLabel();
        lblFuncao = new javax.swing.JLabel();
        btnListar = new javax.swing.JButton();
        lblTelefone = new javax.swing.JLabel();
        lblDataAdmissao = new javax.swing.JLabel();
        lblDataDemissao = new javax.swing.JLabel();
        lblSalario = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblSenha = new javax.swing.JLabel();
        ftxtDataAdmissao = new javax.swing.JFormattedTextField();
        ftxtDataDemissao = new javax.swing.JFormattedTextField();
        txtFuncaoNome = new javax.swing.JTextField();
        txtNome = new javax.swing.JTextField();
        txtSalario = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        pwdSenha = new javax.swing.JPasswordField();
        btnFechar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        ftxtTelefone = new javax.swing.JFormattedTextField();
        txtFuncaoId = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblNome.setText("Nome*");

        lblFuncao.setText("Função*");

        btnListar.setText("Listar");
        btnListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListarActionPerformed(evt);
            }
        });

        lblTelefone.setText("Telefone");

        lblDataAdmissao.setText("Data Admissão*");

        lblDataDemissao.setText("Data Demissão");

        lblSalario.setText("Salário*");

        lblUsuario.setText("Usuário*");

        lblSenha.setText("Senha*");

        ftxtDataAdmissao.setMargin(new java.awt.Insets(0, 5, 0, 0));

        ftxtDataDemissao.setMargin(new java.awt.Insets(0, 5, 0, 0));

        txtFuncaoNome.setEditable(false);
        txtFuncaoNome.setMargin(new java.awt.Insets(0, 5, 0, 0));

        txtNome.setMargin(new java.awt.Insets(0, 5, 0, 0));
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNomeKeyReleased(evt);
            }
        });

        txtSalario.setMargin(new java.awt.Insets(0, 5, 0, 0));

        txtUsuario.setMargin(new java.awt.Insets(0, 5, 0, 0));

        pwdSenha.setMargin(new java.awt.Insets(0, 5, 0, 0));

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        ftxtTelefone.setMargin(new java.awt.Insets(0, 5, 0, 0));

        txtFuncaoId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtFuncaoIdFocusLost(evt);
            }
        });
        txtFuncaoId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFuncaoIdKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel4.setText("* = Campo obrigatorio");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDataAdmissao)
                            .addComponent(lblTelefone)
                            .addComponent(lblSalario)
                            .addComponent(lblUsuario)
                            .addComponent(lblSenha)
                            .addComponent(lblFuncao)
                            .addComponent(lblNome)
                            .addComponent(lblDataDemissao))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ftxtDataDemissao)
                            .addComponent(txtSalario)
                            .addComponent(txtUsuario)
                            .addComponent(pwdSenha)
                            .addComponent(txtNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ftxtTelefone, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ftxtDataAdmissao)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(txtFuncaoId, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFuncaoNome, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)))
                        .addGap(7, 7, 7)
                        .addComponent(btnListar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFuncao)
                    .addComponent(btnListar)
                    .addComponent(txtFuncaoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFuncaoId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTelefone)
                    .addComponent(ftxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDataAdmissao)
                    .addComponent(ftxtDataAdmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDataDemissao)
                    .addComponent(ftxtDataDemissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSalario)
                    .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUsuario)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSenha)
                    .addComponent(pwdSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnSalvar)
                    .addComponent(jLabel4))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        funcionario = new Funcionario();

        String nome = txtNome.getText();
        boolean validNomeVazio = validacao.vazio(nome);

        boolean validFuncaoVazio = validacao.vazio(txtFuncaoId.getText());

        String dataAdmissao = ftxtDataAdmissao.getText();
        boolean validDataAdmissao;
        if (validacao.vazio(dataAdmissao)) {
            validDataAdmissao = false;
        } else {
            validDataAdmissao = validarData(ftxtDataAdmissao);
        }

        String dataDemissao = ftxtDataDemissao.getText();
        boolean validDataDemissao;
        if (validacao.vazio(dataDemissao)) {
            validDataDemissao = true;
            dataDemissao = "";
        } else {
            validDataDemissao = validarData(ftxtDataDemissao);
        }

        boolean validDatas = true;
        if (validDataAdmissao && validDataDemissao && !validacao.vazio(dataDemissao)) {
            try {
                String inicio = dataAdmissao;
                String fim = dataDemissao;

                String pattern = "dd/MM/yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                Date dataInicio = simpleDateFormat.parse(inicio);
                Date dataFim = simpleDateFormat.parse(fim);

                if (dataInicio.before(dataFim) || dataInicio.equals(dataFim)) {
                    validDatas = true;
                } else {
                    validDatas = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                validDatas = false;
            }
        }

        Double salario = 0.0;
        if (!Validacao.vazio(txtSalario.getText())) {
            salario = Double.valueOf(txtSalario.getText());
        }
        boolean validSalarioVazio = (validacao.vazio(String.valueOf(salario)) || salario == 0.0);

        String login = txtUsuario.getText().toUpperCase();
        boolean validLoginVazio = validacao.vazio(login);
        boolean validLoginExistente = false;
        if (!validLoginVazio && codigo == 0) {
            validLoginExistente = new FuncionarioDAO().existeLogin(login);
        }

        // Encriptar senha
        String senha = String.valueOf(pwdSenha.getPassword());
        boolean validSenhaVazio = validacao.vazio(senha);
        if (!validSenhaVazio) {
            senha = Encriptar.md5(senha);
        }

        if (codigo != 0) {
            validSenhaVazio = false;
        }

        String telefone = ftxtTelefone.getText();

        if (!validNomeVazio
                && !validFuncaoVazio
                && validDataAdmissao && validDataDemissao && validDatas
                && !validLoginExistente && !validLoginVazio
                && !validSenhaVazio
                && !validSalarioVazio) {

            int funcaoId = Integer.valueOf(txtFuncaoId.getText());

            // Setters
            funcionario.setNome(nome);
            funcionario.setFuncao_id(funcaoId);
            funcionario.setTelefone(telefone);
            funcionario.setData_admissao(dataAdmissao);
            funcionario.setData_demissao(dataDemissao);
            funcionario.setSalario(salario);
            funcionario.setLogin(login);
            funcionario.setSenha(senha);

            if (Validacao.vazio(dataDemissao)) {
                funcionario.setSituacao('A');
            } else {
                funcionario.setSituacao('I');
            }

            FuncionarioDAO dao = new FuncionarioDAO();
            String retorno = null;

            if (codigo == 0) {
                if (funcionario.getSituacao() == 'A') {
                    retorno = dao.salvarSemData(funcionario);
                } else {
                    retorno = dao.salvar(funcionario);
                }
            } else {
                funcionario.setId(codigo);

                retorno = dao.atualizar(funcionario);

                if (funcionario.getSituacao() == 'I') {
                    retorno = dao.atualizarDataDemissao(funcionario);
                } else {
                    retorno = dao.removerDataDemissao(funcionario);
                }
                if (!Validacao.vazio(senha)) {
                    retorno = dao.atualizarSenha(funcionario);
                }

            }

            if (retorno == null) {
                JOptionPane.showMessageDialog(null, "Registro salvo com sucesso!");

                // Limpar campos
                txtNome.setText("");
                txtFuncaoId.setText("");
                txtFuncaoNome.setText("");
                ftxtTelefone.setText("");
                ftxtDataAdmissao.setText("");
                ftxtDataDemissao.setText("");
                txtSalario.setText("");
                txtUsuario.setText("");
                pwdSenha.setText("");

                // Remover autocomplete
                Formatacao.formatarData(ftxtDataAdmissao);
                Formatacao.formatarData(ftxtDataDemissao);
                Formatacao.formatarTelefone(ftxtTelefone);

                // Posicionar cursor
                txtNome.requestFocus();

                txtNome.setBackground(Color.WHITE);
                txtFuncaoId.setBackground(Color.WHITE);
                txtFuncaoNome.setBackground(Color.WHITE);
                ftxtTelefone.setBackground(Color.WHITE);
                ftxtDataAdmissao.setBackground(Color.WHITE);
                ftxtDataDemissao.setBackground(Color.WHITE);
                txtSalario.setBackground(Color.WHITE);
                txtUsuario.setBackground(Color.WHITE);
                pwdSenha.setBackground(Color.WHITE);

                janelaPai.atualizarTabela();

                if (codigo != 0) {
                    this.dispose();
                }

                this.codigo = 0;
            } else {
                JOptionPane.showMessageDialog(null, "Ops! Problemas ao salvar registro.");
            }
        } else {
            String erro = "";

            if (validNomeVazio) {
                erro += "Campo NOME inválido! Campo não pode estar vazio.\n";
                txtNome.setBackground(Color.PINK);
            } else {
                txtNome.setBackground(Color.WHITE);
            }

            if (validFuncaoVazio) {
                erro += "Campo FUNCÃO inválido! Favor selecionar uma função.\n";
                txtFuncaoNome.setBackground(Color.PINK);
            } else {
                txtFuncaoNome.setBackground(Color.WHITE);
            }

            if (!validDatas) {
                erro += "A data de DEMISSÃO não pode vir antes da ADMISSÃO!";
                ftxtDataAdmissao.setBackground(Color.PINK);
                ftxtDataDemissao.setBackground(Color.PINK);
            } else {
                ftxtDataAdmissao.setBackground(Color.WHITE);
                ftxtDataDemissao.setBackground(Color.WHITE);
            }

            if (!validDataAdmissao) {
                erro += "Campo DATA ADMISSÃO inválido! Insira uma data válida.\n";
                ftxtDataAdmissao.setBackground(Color.PINK);
            } else {
                ftxtDataAdmissao.setBackground(Color.WHITE);
            }

            if (!validDataDemissao) {
                erro += "Campo DATA DEMISSÃO inválido! Insira uma data válida.\n";
                ftxtDataDemissao.setBackground(Color.PINK);
                Formatacao.formatarData(ftxtDataDemissao);
            } else {
                ftxtDataDemissao.setBackground(Color.WHITE);
            }

            if (validLoginExistente || validLoginVazio) {
                if (validLoginExistente) {
                    erro += "Campo USUÁRIO inválido! Usuário já está sendo utilizado.\n";
                }
                if (validLoginVazio) {
                    erro += "Campo USUÁRIO inválido! Campo não pode estar vazio.\n";
                }
                txtUsuario.setBackground(Color.PINK);
            } else {
                txtUsuario.setBackground(Color.WHITE);
            }

            if (validSenhaVazio) {
                erro += "Campo SENHA inválido! Campo não pode estar vazio.\n";
                pwdSenha.setBackground(Color.PINK);
            } else {
                pwdSenha.setBackground(Color.WHITE);
            }

            if (validSalarioVazio) {
                erro += "Campo SALÁRIO inválido! Campo não pode estar vazio.\n";
                txtSalario.setBackground(Color.PINK);
            } else {
                txtSalario.setBackground(Color.WHITE);
            }

            JOptionPane.showMessageDialog(this, erro);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    public boolean validarData(JFormattedTextField textField) {
        boolean retorno;
        try {
            String data = textField.getText();
            boolean valido = Validacao.validarDataFormatada(data);

            if (!valido) {
                retorno = false;
            } else {
                retorno = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            retorno = false;
        }
        return retorno;
    }

    private void btnListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListarActionPerformed
        new DlgListarFuncao(this, true, this).setVisible(true);
    }//GEN-LAST:event_btnListarActionPerformed

    private void txtFuncaoIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFuncaoIdKeyReleased
        validarNumeros(txtFuncaoId);
    }//GEN-LAST:event_txtFuncaoIdKeyReleased

    private void txtFuncaoIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFuncaoIdFocusLost
        String texto = txtFuncaoId.getText().trim();
        if (!texto.equals("")) {
            int id = Integer.parseInt(texto);
            Funcao f = new FuncaoDAO().consultarId(id);

            if (f != null) {
                setTxtFuncao(f.getNome(), String.valueOf(f.getId()));
            } else {
                txtFuncaoId.setText("");
                txtFuncaoNome.setText("");

                txtFuncaoId.setEditable(true);
                txtFuncaoId.requestFocus();

                JOptionPane.showMessageDialog(this, "ID não existende");
            }
        }
    }//GEN-LAST:event_txtFuncaoIdFocusLost

    private void txtNomeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyReleased
        validarLetras(txtNome);
    }//GEN-LAST:event_txtNomeKeyReleased

    public void setTxtFuncao(String nome, String id) {
        txtFuncaoNome.setText(nome);
        txtFuncaoId.setText(id);

        txtFuncaoNome.setEditable(false);
        txtFuncaoId.setEditable(false);
    }

    public String validarNumeros(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temLetras(texto)) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    public String validarLetras(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temNumeros(texto)) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgFuncionario dialog = new DlgFuncionario(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnListar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JFormattedTextField ftxtDataAdmissao;
    private javax.swing.JFormattedTextField ftxtDataDemissao;
    private javax.swing.JFormattedTextField ftxtTelefone;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JLabel lblDataAdmissao;
    private javax.swing.JLabel lblDataDemissao;
    private javax.swing.JLabel lblFuncao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblSalario;
    private javax.swing.JLabel lblSenha;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPasswordField pwdSenha;
    private javax.swing.JTextField txtFuncaoId;
    private javax.swing.JTextField txtFuncaoNome;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtSalario;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
