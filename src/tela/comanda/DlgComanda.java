package tela.comanda;

/*
 * @author fritzzin
 */
import apoio.Formatacao;
import apoio.Validacao;
import dao.ClienteDAO;
import dao.ComandaDAO;
import dao.ItemComandaDAO;
import dao.ProdutoDAO;
import entidade.Cliente;
import entidade.Comanda;
import entidade.ItemComanda;
import entidade.Produto;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DlgComanda extends javax.swing.JDialog {

    Validacao validacao;
    Comanda comanda;
    ComandaDAO comandaDao;
    ItemComandaDAO itemDao;
    int idComandaAtual;
    int idFuncionario;
    String data;
    double valorTotal;
    boolean editando;

    public DlgComanda(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
    }

    public DlgComanda(java.awt.Frame parent, boolean modal, int idFuncionario) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        this.setTitle("Nova Comanda");
        this.validacao = new Validacao();
        this.comandaDao = new ComandaDAO();
        this.comanda = new Comanda();
        this.idFuncionario = idFuncionario;
        this.itemDao = new ItemComandaDAO();
        this.editando = false;

        this.data = Formatacao.getDataAtual();
        lbl_data.setText("Data: " + this.data);

        txtProdutoId.requestFocus();

        // Criar nova comanda
        this.comanda.setCliente_id(1);
        this.comanda.setFuncionario_id(idFuncionario);
        this.comanda.setValor(0.0);
        this.comanda.setData(data);
        this.comanda.setMetodo_pagamento("EM ABERTO");
        this.comanda.setSituacao('I');

        comandaDao.salvar(comanda);

        // Verificar se é a primeira comanda a ser criada
        if (comandaDao.getId() == 0) {
            this.idComandaAtual = 1;
        } else {
            this.idComandaAtual = comandaDao.getId();
        }

        txtClienteId.requestFocus();

        System.out.println("ID COMANDA: " + this.idComandaAtual);
    }

    public DlgComanda(java.awt.Frame parent, boolean modal, Comanda comanda) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        String comandaId = String.valueOf(comanda.getId());
        String clienteId = String.valueOf(comanda.getCliente_id());

        this.setTitle("Editando comanda");
        this.validacao = new Validacao();
        this.comandaDao = new ComandaDAO();
        this.itemDao = new ItemComandaDAO();
        this.editando = true;
        this.comanda = comanda;
        this.idFuncionario = comanda.getFuncionario_id();
        this.idComandaAtual = comanda.getId();
        this.valorTotal = comanda.getValor();

        txtValorTotal.setText(String.valueOf(valorTotal));

        this.data = comanda.getData();
        lbl_data.setText("Data: " + data);

        txtClienteId.setText(clienteId);
        Cliente c = new ClienteDAO().consultarId(comanda.getCliente_id());
        txtClienteNome.setText(c.getNome());

        txtProdutoId.requestFocus();

        switch (comanda.getMetodo_pagamento()) {
            case "CRÉDITO":
                cmbMetodoPagamento.setSelectedIndex(1);
                cmbSituacao.setSelectedItem(0);
                break;
            case "DÉBITO":
                cmbMetodoPagamento.setSelectedIndex(2);
                cmbSituacao.setSelectedItem(0);
                break;
            case "DINHEIRO":
                cmbMetodoPagamento.setSelectedIndex(3);
                cmbSituacao.setSelectedItem(0);
                break;
            case "EM ABERTO":
                cmbMetodoPagamento.setSelectedIndex(4);
                cmbSituacao.setSelectedItem(1);
                break;
            default:
                cmbMetodoPagamento.setSelectedIndex(0);
                cmbSituacao.setSelectedItem(1);
                break;
        }

        btnFechar.setEnabled(false);

        atualizarTabela(comandaId);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtProdutoNome = new javax.swing.JTextField();
        btnFechar = new javax.swing.JButton();
        lbl_qntdValor = new javax.swing.JLabel();
        btnFinalizar = new javax.swing.JButton();
        txtProdutoQuantidade = new javax.swing.JTextField();
        txtProdutoValor = new javax.swing.JTextField();
        btnItemAdicionar = new javax.swing.JButton();
        btnItemRemover = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblItemComanda = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnPesquisarCliente = new javax.swing.JButton();
        txtClienteNome = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        txtProdutoId = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnPesquisarProduto = new javax.swing.JButton();
        txtValorTotal = new javax.swing.JTextField();
        txtClienteId = new javax.swing.JTextField();
        lbl_data = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cmbMetodoPagamento = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        cmbSituacao = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtProdutoNome.setEditable(false);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        lbl_qntdValor.setText("Quantidade e valor:");

        btnFinalizar.setText("Finalizar");
        btnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarActionPerformed(evt);
            }
        });

        txtProdutoQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtProdutoQuantidadeKeyReleased(evt);
            }
        });

        txtProdutoValor.setEditable(false);
        txtProdutoValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtProdutoValorKeyReleased(evt);
            }
        });

        btnItemAdicionar.setText("Adicionar Item");
        btnItemAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnItemAdicionarActionPerformed(evt);
            }
        });

        btnItemRemover.setText("Remover Item");
        btnItemRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnItemRemoverActionPerformed(evt);
            }
        });

        tblItemComanda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String [] {
                "Nome", "Quantidade", "Valor Unitário"
            }
        ));
        jScrollPane1.setViewportView(tblItemComanda);

        jLabel2.setText("Cliente*:");

        btnPesquisarCliente.setText("Pesquisar");
        btnPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarClienteActionPerformed(evt);
            }
        });

        txtClienteNome.setEditable(false);

        jLabel3.setText("Produto:");

        txtProdutoId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtProdutoIdFocusLost(evt);
            }
        });
        txtProdutoId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtProdutoIdKeyReleased(evt);
            }
        });

        jLabel6.setText("Valor Total:");

        btnPesquisarProduto.setText("Pesquisar");
        btnPesquisarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarProdutoActionPerformed(evt);
            }
        });

        txtValorTotal.setEditable(false);
        txtValorTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtClienteId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtClienteIdFocusLost(evt);
            }
        });
        txtClienteId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtClienteIdKeyReleased(evt);
            }
        });

        lbl_data.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_data.setText("DATA");

        jLabel1.setText("Método Pagamento*:");

        cmbMetodoPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecionar", "CRÉDITO", "DÉBITO", "DINHEIRO", "EM ABERTO" }));
        cmbMetodoPagamento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMetodoPagamentoItemStateChanged(evt);
            }
        });

        jLabel4.setText("Situção*:");

        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "EM ABERTO", "PAGO"}));
        cmbSituacao.setEnabled(false);

        jLabel8.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel8.setText("* = Campo obrigatorio");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_data, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_qntdValor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtProdutoQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtProdutoValor))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(29, 29, 29)
                        .addComponent(txtClienteId, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtClienteNome, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPesquisarCliente))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtValorTotal)
                            .addComponent(cmbSituacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(cmbMetodoPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFinalizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(29, 29, 29)
                                .addComponent(txtProdutoId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtProdutoNome, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPesquisarProduto))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnItemAdicionar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnItemRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lbl_data)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtClienteNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisarCliente)
                    .addComponent(txtClienteId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtProdutoId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdutoNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisarProduto))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_qntdValor)
                    .addComponent(txtProdutoQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdutoValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnItemRemover)
                    .addComponent(btnItemAdicionar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMetodoPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnFinalizar)
                    .addComponent(jLabel8))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        if (editando) {
            btnFinalizarActionPerformed(evt);
        } else {
            new ItemComandaDAO().excluirTodos(idComandaAtual);
            comandaDao.excluir(idComandaAtual);
        }
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarClienteActionPerformed
        new DlgListarCliente(this, true, new Cliente(), this).setVisible(true);
    }//GEN-LAST:event_btnPesquisarClienteActionPerformed

    private void txtClienteIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClienteIdKeyReleased
        validarNumeros(txtClienteId);
    }//GEN-LAST:event_txtClienteIdKeyReleased

    private void txtClienteIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtClienteIdFocusLost
        String texto = txtClienteId.getText().trim();
        if (!texto.equals("")) {

            int id = Integer.parseInt(texto);
            Cliente c = new ClienteDAO().consultarId(id);

            if (c != null) {
                setParametrosCliente(c);
            } else {
                txtClienteId.setText("");
                txtClienteNome.setText("");

                txtClienteId.setEditable(true);
                btnPesquisarCliente.setEnabled(true);

                txtClienteId.requestFocus();

                JOptionPane.showMessageDialog(this, "ID não existende");
            }
        }
    }//GEN-LAST:event_txtClienteIdFocusLost

    private void txtProdutoIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProdutoIdKeyReleased
        validarNumeros(txtProdutoId);

        if (txtProdutoId.getText().equals("1")) {
            txtProdutoValor.setEditable(true);
        } else {
            txtProdutoValor.setEditable(false);
        }
    }//GEN-LAST:event_txtProdutoIdKeyReleased

    private void txtProdutoIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtProdutoIdFocusLost
        String texto = txtProdutoId.getText().trim();

        if (!texto.equals("")) {

            int id = Integer.parseInt(texto);
            Produto p = new ProdutoDAO().consultarId(id);

            if (p != null) {
                setParametrosProduto(p);
            } else {
                txtProdutoId.setText("");
                txtProdutoNome.setText("");
                txtProdutoValor.setText("");
                txtProdutoQuantidade.setText("");

                txtProdutoId.requestFocus();

                JOptionPane.showMessageDialog(this, "ID não existente");
            }
        }
    }//GEN-LAST:event_txtProdutoIdFocusLost

    private void btnPesquisarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarProdutoActionPerformed
        new DlgListarProduto(this, true, new Produto(), this).setVisible(true);
    }//GEN-LAST:event_btnPesquisarProdutoActionPerformed

    private void btnItemAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnItemAdicionarActionPerformed

        boolean vazioValor = Validacao.vazio(txtProdutoValor.getText());
        boolean vazioId = Validacao.vazio(txtProdutoId.getText());
        boolean vazioQtde = Validacao.vazio(txtProdutoQuantidade.getText());

        if (!vazioId && !vazioQtde && !vazioValor) {
            ItemComanda item = new ItemComanda();

            int idComanda = this.idComandaAtual;
            int idProduto = Integer.parseInt(txtProdutoId.getText());
            int quantidade = Integer.parseInt(txtProdutoQuantidade.getText());
            double valor = Double.parseDouble(txtProdutoValor.getText());
            char situacao = 'A';

            item.setId(WIDTH);
            item.setComanda_id(idComanda);
            item.setProduto_id(idProduto);
            item.setQtde(quantidade);
            item.setValor(valor);
            item.setSituacao(situacao);

            String retorno = null;

            retorno = itemDao.salvar(item);

            if (retorno == null) {

                txtProdutoId.setText("");
                txtProdutoNome.setText("");
                txtProdutoQuantidade.setText("");
                txtProdutoValor.setText("");

                txtProdutoId.setBackground(Color.WHITE);
                txtProdutoQuantidade.setBackground(Color.WHITE);
                txtProdutoValor.setBackground(Color.WHITE);

                txtProdutoValor.setEditable(false);

                txtProdutoId.requestFocus();

                atualizarTabela(String.valueOf(idComanda));
            }
        } else {
            String erro = "";

            if (vazioId) {
                erro += "Campo ID inválido. Favor inserir um ID válido.";
                txtProdutoId.setBackground(Color.PINK);
            } else {
                txtProdutoId.setBackground(Color.WHITE);
            }
            if (vazioValor) {
                erro += "Campo VALOR inválido. Favor inserir um valor válido.";
                txtProdutoValor.setBackground(Color.PINK);
            } else {
                txtProdutoValor.setBackground(Color.WHITE);
            }
            if (vazioQtde) {
                erro += "Campo QUANTIDADE inválido. Favor inserir uma quantidade válida.";
                txtProdutoQuantidade.setBackground(Color.PINK);
            } else {
                txtProdutoQuantidade.setBackground(Color.WHITE);
            }
            JOptionPane.showMessageDialog(this, erro);
        }
    }//GEN-LAST:event_btnItemAdicionarActionPerformed

    private void txtProdutoQuantidadeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProdutoQuantidadeKeyReleased
        validarNumeros(txtProdutoQuantidade);
    }//GEN-LAST:event_txtProdutoQuantidadeKeyReleased

    private void txtProdutoValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProdutoValorKeyReleased
        validarNumeros(txtProdutoValor);
    }//GEN-LAST:event_txtProdutoValorKeyReleased

    private void btnItemRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnItemRemoverActionPerformed
        int idItem = Integer.parseInt((String) tblItemComanda.getValueAt(tblItemComanda.getSelectedRow(), 0));

        double valor = (double) tblItemComanda.getValueAt(tblItemComanda.getSelectedRow(), 3);
        System.out.println("VALOR: " + valor);

        itemDao.excluirItem(idItem);

        atualizarTabela(String.valueOf(idComandaAtual));
    }//GEN-LAST:event_btnItemRemoverActionPerformed

    private void btnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarActionPerformed
        comanda = new Comanda();

        String erro = "";

        char situacao;
        if (cmbSituacao.getSelectedItem().equals("PAGO")) {
            situacao = 'A';
        } else {
            situacao = 'I';
        }

        if (!txtClienteId.getText().equals("")) {
            int idCliente = Integer.parseInt(txtClienteId.getText());
            String metodoPagamento = (String) cmbMetodoPagamento.getSelectedItem();

            boolean validarCliente = !txtClienteId.equals("");
            boolean validarMetodoPagamento = !metodoPagamento.equals("Selecionar");
            boolean validarItensComanda = new ItemComandaDAO().qntdItens(this.idComandaAtual) != 0;
            boolean clienteBalcaoSemMetodo = (metodoPagamento.equals("EM ABERTO") && idCliente == 1);

            if (validarMetodoPagamento && validarItensComanda && !clienteBalcaoSemMetodo && validarCliente) {
                comanda.setCliente_id(idCliente);
                comanda.setData(data);
                comanda.setFuncionario_id(idFuncionario);
                comanda.setMetodo_pagamento(metodoPagamento);
                comanda.setSituacao(situacao);
                comanda.setValor(valorTotal);
                comanda.setId(idComandaAtual);

                comandaDao = new ComandaDAO();
                String retorno = null;

                retorno = comandaDao.atualizar(comanda);

                if (retorno == null) {
                    JOptionPane.showMessageDialog(this, "Registro salvo com sucesso!");
                    this.dispose();
                } else {
                    erro += "Ops! Problemas ao salvar registro: \n";
                }
            } else {
                erro += "Problema(s) ao salvar o registro : \n";

                txtClienteId.setBackground(Color.WHITE);
                cmbMetodoPagamento.setBackground(Color.WHITE);
                txtProdutoId.setBackground(Color.WHITE);

                if (!validarCliente) {
                    erro += "  - É necessário escolher um cliente.\n";
                    txtClienteId.setBackground(Color.PINK);
                }
                if (!validarItensComanda) {
                    erro += "  - Itens são necessários para a comanda.\n";
                    txtProdutoId.setBackground(Color.PINK);
                }
                if (!validarMetodoPagamento) {
                    erro += "  - Favor escolher a forma de pagamento.\n";
                    cmbMetodoPagamento.setBackground(Color.PINK);
                }
                if (clienteBalcaoSemMetodo) {
                    erro += "  - BALCÃO não pode deixar pagamento EM ABERTO.\n";
                    cmbMetodoPagamento.setBackground(Color.PINK);
                }
            }
        } else {
            erro += "  - É necessário escolher um cliente\n";
            txtClienteId.setBackground(Color.PINK);
        }

        if (!Validacao.vazio(erro)) {
            JOptionPane.showMessageDialog(this, erro);
            erro = "";
        }
    }//GEN-LAST:event_btnFinalizarActionPerformed

    private void cmbMetodoPagamentoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMetodoPagamentoItemStateChanged
        boolean selecionar = cmbMetodoPagamento.getSelectedItem().equals("Selecionar");
        boolean naoDefinido = cmbMetodoPagamento.getSelectedItem().equals("EM ABERTO");
        if (selecionar || naoDefinido) {
            cmbSituacao.setSelectedIndex(0);
        } else {
            cmbSituacao.setSelectedIndex(1);
        }
    }//GEN-LAST:event_cmbMetodoPagamentoItemStateChanged

    public String validarNumeros(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temLetras(texto)) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    public void setParametrosCliente(Cliente c) {
        String id = String.valueOf(c.getId());
        String nome = c.getNome();

        txtClienteId.setText(id);
        txtClienteNome.setText(nome);

        txtClienteId.setEditable(false);
        txtClienteId.setBackground(Color.WHITE);
        btnPesquisarCliente.setEnabled(false);

    }

    public void setParametrosProduto(Produto p) {
        String id = String.valueOf(p.getId());
        String nome = p.getNome();
        String preco = String.valueOf(p.getPreco_uni());

        txtProdutoId.setText(id);
        txtProdutoNome.setText(nome);
        txtProdutoValor.setText(preco);
        txtProdutoQuantidade.setText("1");

        // Se item for Buffet a Kg
        if (p.getId() == 1) {
            txtProdutoValor.setEditable(true);
        } else {
            txtProdutoValor.setEditable(false);
        }
    }

    public void atualizarTabela(String criterio) {
        new ItemComandaDAO().popularTabela(tblItemComanda, criterio, 'A', this);
    }

    public void setValorTotal(double valor) {
        this.valorTotal = valor;
        txtValorTotal.setText(String.valueOf(valorTotal));
    }

    public void desabilitarCliente() {
        txtClienteId.setEditable(false);
        btnPesquisarCliente.setEnabled(false);
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgComanda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgComanda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgComanda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgComanda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgComanda dialog = new DlgComanda(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnFinalizar;
    private javax.swing.JButton btnItemAdicionar;
    private javax.swing.JButton btnItemRemover;
    private javax.swing.JButton btnPesquisarCliente;
    private javax.swing.JButton btnPesquisarProduto;
    private javax.swing.JComboBox<String> cmbMetodoPagamento;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbl_data;
    private javax.swing.JLabel lbl_qntdValor;
    private javax.swing.JTable tblItemComanda;
    private javax.swing.JTextField txtClienteId;
    private javax.swing.JTextField txtClienteNome;
    private javax.swing.JTextField txtProdutoId;
    private javax.swing.JTextField txtProdutoNome;
    private javax.swing.JTextField txtProdutoQuantidade;
    private javax.swing.JTextField txtProdutoValor;
    private javax.swing.JTextField txtValorTotal;
    // End of variables declaration//GEN-END:variables
}
