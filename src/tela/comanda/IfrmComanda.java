package tela.comanda;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.Formatacao;
import apoio.Validacao;
import dao.ClienteDAO;
import dao.ComandaDAO;
import entidade.Cliente;
import entidade.Comanda;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class IfrmComanda extends javax.swing.JInternalFrame {

    int codigo = 0;
    int idFuncionario;

    public IfrmComanda(int idFuncionario) {
        initComponents();
        this.setTitle("Comandas");

        this.idFuncionario = idFuncionario;

        Formatacao.formatarData(ftxtDataInicio);
        Formatacao.formatarData(ftxtDataFim);

        ftxtDataFim.setText(Formatacao.getDataAtual());
        ftxtDataInicio.setText(Formatacao.getDataSemanaPassada());

        atualizarTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbSituacao = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblComanda = new javax.swing.JTable();
        btnFechar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        ftxtDataInicio = new javax.swing.JFormattedTextField();
        ftxtDataFim = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtClienteNome = new javax.swing.JTextField();
        txtClienteId = new javax.swing.JTextField();
        btnPesquisarCliente = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtQuantidade = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtValorTotal = new javax.swing.JTextField();

        cmbSituacao.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        cmbSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pago", "Em aberto"}));
        cmbSituacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSituacaoItemStateChanged(evt);
            }
        });

        jLabel2.setText("Situação");

        tblComanda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblComanda);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnImprimir.setText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        jLabel3.setText("Data início:");

        jLabel4.setText("Data fim:");

        ftxtDataInicio.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ftxtDataInicioFocusLost(evt);
            }
        });

        ftxtDataFim.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ftxtDataFimFocusLost(evt);
            }
        });

        jLabel5.setText("Cliente");

        txtClienteNome.setEditable(false);

        txtClienteId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtClienteIdFocusLost(evt);
            }
        });
        txtClienteId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtClienteIdKeyReleased(evt);
            }
        });

        btnPesquisarCliente.setText("Pesquisar Cliente");
        btnPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarClienteActionPerformed(evt);
            }
        });

        jLabel1.setText("Quantidade:");

        txtQuantidade.setEditable(false);

        jLabel6.setText("Valor total:");

        txtValorTotal.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnNovo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnImprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFechar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel2))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(ftxtDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(30, 30, 30)
                                            .addComponent(jLabel4)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(ftxtDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(txtClienteId, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtClienteNome, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnPesquisarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(ftxtDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(ftxtDataFim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtClienteNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClienteId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisarCliente))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtValorTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar)
                    .addComponent(btnNovo)
                    .addComponent(btnEditar)
                    .addComponent(btnImprimir))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbSituacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSituacaoItemStateChanged
        atualizarTabela();
    }//GEN-LAST:event_cmbSituacaoItemStateChanged

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        new DlgComanda(null, true, idFuncionario).setVisible(true);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        String idString = String.valueOf(tblComanda.getValueAt(tblComanda.getSelectedRow(), 0));
        int id = Integer.parseInt(idString);

        Comanda c = new ComandaDAO().consultarId(id);

        if (c == null) {
            JOptionPane.showMessageDialog(null, "Registro não localizado!");
        } else {
            codigo = c.getId();
            new DlgComanda(null, true, c).setVisible(true);
        }
        atualizarTabela();
        codigo = 0;
    }//GEN-LAST:event_btnEditarActionPerformed

    private void txtClienteIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClienteIdKeyReleased
        validarNumeros(txtClienteId);
    }//GEN-LAST:event_txtClienteIdKeyReleased

    private void txtClienteIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtClienteIdFocusLost
        String texto = txtClienteId.getText().trim();
        if (!texto.equals("")) {

            int id = Integer.parseInt(texto);
            Cliente c = new ClienteDAO().consultarId(id);

            if (c != null) {
                setParametrosCliente(c);
                atualizarTabela();
            } else {
                txtClienteId.setText("");
                txtClienteNome.setText("");

                txtClienteId.requestFocus();

                JOptionPane.showMessageDialog(this, "ID não existende");
            }
        } else {
            txtClienteId.setText("");
            txtClienteNome.setText("");
            atualizarTabela();
        }
    }//GEN-LAST:event_txtClienteIdFocusLost

    private void btnPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarClienteActionPerformed
        new DlgListarCliente(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnPesquisarClienteActionPerformed

    private void ftxtDataInicioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ftxtDataInicioFocusLost
        if (Validacao.vazio(ftxtDataInicio.getText())) {
            Formatacao.formatarData(ftxtDataInicio);
            ftxtDataInicio.setBackground(Color.WHITE);
            atualizarTabela();
        } else {
            validarData(ftxtDataInicio);
            atualizarTabela();
        }
    }//GEN-LAST:event_ftxtDataInicioFocusLost

    private void ftxtDataFimFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ftxtDataFimFocusLost
        if (Validacao.vazio(ftxtDataFim.getText()) || validarDataFim()) {
            if (Validacao.vazio(ftxtDataFim.getText())) {
                Formatacao.formatarData(ftxtDataFim);
            }
            ftxtDataFim.setBackground(Color.WHITE);
            atualizarTabela();
        } else {
            if (validarData(ftxtDataFim)) {
                if (validarDataFim()) {
                    atualizarTabela();
                } else {
                    JOptionPane.showMessageDialog(this, "A data final deve vir APÓS a inicial");
                    ftxtDataFim.setBackground(Color.PINK);
                }
            }
        }
    }//GEN-LAST:event_ftxtDataFimFocusLost

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        if (Validacao.vazio(ftxtDataFim.getText()) || Validacao.vazio(ftxtDataInicio.getText())) {
            if (Validacao.vazio(txtClienteId.getText())) {
                relatorioSituacao();
            } else {
                relatorioCliente();
            }
        } else {
            boolean validarInicio = validarData(ftxtDataInicio);
            boolean validarFim = validarData(ftxtDataFim);
            if (validarInicio && validarFim && Validacao.vazio(txtClienteNome.getText())) {
                relatorioData();
            } else {
                relatorioClienteData();
            }
        }
    }//GEN-LAST:event_btnImprimirActionPerformed

    public void relatorioSituacao() {
        char opcao;
        if (cmbSituacao.getSelectedItem().equals("Pago")) {
            opcao = 'A';
        } else {
            opcao = 'I';
        }
        try {
            // Compila o relatorio
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/reports/comanda/comandaSituacao.jrxml"));

            // Mapeia campos de parametros para o relatorio, mesmo que nao existam
            Map parametros = new HashMap();

            // adiciona parametros
            parametros.put("my_situacao", opcao);

            // Executa relatoio
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

            // Exibe resultado em video
            JasperViewer.viewReport(impressao, false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public void relatorioCliente() {
        char opcao;
        if (cmbSituacao.getSelectedItem().equals("Pago")) {
            opcao = 'A';
        } else {
            opcao = 'I';
        }

        String nome = txtClienteNome.getText();

        try {
            // Compila o relatorio
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/reports/comanda/comandaCliente.jrxml"));

            // Mapeia campos de parametros para o relatorio, mesmo que nao existam
            Map parametros = new HashMap();

            // adiciona parametros
            parametros.put("my_situacao", opcao);
            parametros.put("my_nome", nome);

            // Executa relatoio
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

            // Exibe resultado em video
            JasperViewer.viewReport(impressao, false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public void relatorioData() {
        try {
            char opcao;
            if (cmbSituacao.getSelectedItem().equals("Pago")) {
                opcao = 'A';
            } else {
                opcao = 'I';
            }
            
            String inicio = ftxtDataInicio.getText();
            String fim = ftxtDataFim.getText();

            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            Date dataInicio = simpleDateFormat.parse(inicio);
            Date dataFim = simpleDateFormat.parse(fim);

            if (dataInicio.before(dataFim) || dataInicio.equals(dataFim)) {

                // Compila o relatorio
                JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/reports/comanda/comandaDataSituacao.jrxml"));

                // Mapeia campos de parametros para o relatorio, mesmo que nao existam
                Map parametros = new HashMap();

                // adiciona parametros
                parametros.put("dataInicio", dataInicio);
                parametros.put("dataFim", dataFim);
                parametros.put("situacao", opcao);

                // Executa relatoio
                JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

                // Exibe resultado em video
                JasperViewer.viewReport(impressao, false);

            } else {
                Formatacao.formatarData(ftxtDataInicio);
                JOptionPane.showMessageDialog(null, "Data de início deve vir ANTES da data final");

                ftxtDataInicio.setBackground(Color.PINK);
                ftxtDataInicio.requestFocus();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public void relatorioClienteData() {
        try {
            char opcao;
            if (cmbSituacao.getSelectedItem().equals("Pago")) {
                opcao = 'A';
            } else {
                opcao = 'I';
            }
            
            String nome = txtClienteNome.getText();
            
            String inicio = ftxtDataInicio.getText();
            String fim = ftxtDataFim.getText();

            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            Date dataInicio = simpleDateFormat.parse(inicio);
            Date dataFim = simpleDateFormat.parse(fim);

            if (dataInicio.before(dataFim) || dataInicio.equals(dataFim)) {

                // Compila o relatorio
                JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/reports/comanda/comandaClienteDataSituacao.jrxml"));

                // Mapeia campos de parametros para o relatorio, mesmo que nao existam
                Map parametros = new HashMap();

                // adiciona parametros
                parametros.put("dataInicio", dataInicio);
                parametros.put("dataFim", dataFim);
                parametros.put("situacao", opcao);
                parametros.put("cliente", nome);

                // Executa relatoio
                JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

                // Exibe resultado em video
                JasperViewer.viewReport(impressao, false);

            } else {
                Formatacao.formatarData(ftxtDataInicio);
                JOptionPane.showMessageDialog(null, "Data de início deve vir ANTES da data final");

                ftxtDataInicio.setBackground(Color.PINK);
                ftxtDataInicio.requestFocus();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public boolean validarDataFim() {
        try {
            String inicio = ftxtDataInicio.getText();
            String fim = ftxtDataFim.getText();

            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            Date dataInicio = simpleDateFormat.parse(inicio);
            Date dataFim = simpleDateFormat.parse(fim);

            if (dataInicio.before(dataFim) || dataInicio.equals(dataFim)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean validarData(JFormattedTextField textField) {
        try {
            String data = textField.getText();
            boolean valido = Validacao.validarDataFormatada(data);

            if (!valido) {
                JOptionPane.showMessageDialog(this, "Favor inserir data válida (Dia/Mês/Ano)");
                Formatacao.formatarData(textField);
                textField.setBackground(Color.PINK);
                textField.requestFocus();
                return false;
            } else {
                textField.setBackground(Color.WHITE);
                return true;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Favor inserir data válida (Dia/Mês/Ano)");
            textField.setBackground(Color.PINK);
            textField.requestFocus();
            e.printStackTrace();
            return false;
        }
    }

    public void atualizarTabela() {
        int clienteId;
        String clienteIdText = txtClienteId.getText();
        if (Validacao.vazio(clienteIdText)) {
            clienteId = 0;
        } else {
            clienteId = Integer.parseInt(clienteIdText);
        }

        char situacao;
        if (cmbSituacao.getSelectedItem() == "Pago") {
            situacao = 'A';
        } else {
            situacao = 'I';
        }

        String dataInicio = ftxtDataInicio.getText();
        String dataFim = ftxtDataFim.getText();

        boolean validDataInicioVazio = Validacao.vazio(dataInicio);
        boolean validDataFimVazio = Validacao.vazio(dataFim);

        if (validDataInicioVazio || validDataFimVazio) {
            if (clienteId == 0) {
                new ComandaDAO().popularTabela(tblComanda, situacao, this);
            } else {
                new ComandaDAO().popularTabela(tblComanda, situacao, clienteId, this);
            }
        } else {
            if (clienteId == 0) {
                new ComandaDAO().popularTabela(tblComanda, dataInicio, dataFim, situacao, this);
            } else {
                new ComandaDAO().popularTabela(tblComanda, dataInicio, dataFim, clienteId, situacao, this);
            }
        }
    }

    public void setParametrosCliente(Cliente c) {
        txtClienteId.setText(String.valueOf(c.getId()));
        txtClienteNome.setText(c.getNome());
    }

    public void setQuantidadeValor(String quantidade, String valorTotal) {
        txtQuantidade.setText(quantidade);
        txtValorTotal.setText(valorTotal);
    }

    public String validarNumeros(JTextField campo) {
        String texto = campo.getText();

        if (Validacao.temLetras(texto)) {
            texto = texto.substring(0, texto.length() - 1);
            campo.setText(texto);
        }
        return texto;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisarCliente;
    private javax.swing.JComboBox<String> cmbSituacao;
    private javax.swing.JFormattedTextField ftxtDataFim;
    private javax.swing.JFormattedTextField ftxtDataInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblComanda;
    private javax.swing.JTextField txtClienteId;
    private javax.swing.JTextField txtClienteNome;
    private javax.swing.JTextField txtQuantidade;
    private javax.swing.JTextField txtValorTotal;
    // End of variables declaration//GEN-END:variables

}
