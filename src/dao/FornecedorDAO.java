package dao;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.IDAO_T;
import entidade.Fornecedor;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class FornecedorDAO implements IDAO_T<Fornecedor> {

    ResultSet resultadoQ = null;
    Fornecedor f;

    @Override
    public String salvar(Fornecedor o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO fornecedor VALUES ("
                    + "DEFAULT, "
                    + "'" + o.getNome().toUpperCase() + "',"
                    + "'" + o.getCnpj() + "',"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar Fornecedor = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(Fornecedor o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

//            System.out.println(o.getId());
            String sql = "UPDATE fornecedor \n"
                    + "SET nome = '" + o.getNome().toUpperCase() + "', \n"
                    + "cnpj     = '" + o.getCnpj() + "', \n"
                    + "situacao = '" + o.getSituacao() + "' \n"
                    + "WHERE id = " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar fornecedor = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE fornecedor "
                    + "SET situacao = 'I' "
                    + "WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar fornecedor = " + e);
            return e.toString();
        }
    }

    public String ativar(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE fornecedor "
                    + "SET situacao = 'A' "
                    + "WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar fornecedor = " + e);
            return e.toString();
        }
    }

    @Override
    public ArrayList<Fornecedor> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Fornecedor> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Fornecedor consultarId(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT * FROM fornecedor "
                    + "WHERE id = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                f = new Fornecedor();

                f.setId(id);
                f.setNome(resultadoQ.getString("nome"));
                f.setCnpj(resultadoQ.getString("cnpj"));
                f.setSituacao(resultadoQ.getString("situacao").charAt(0));
            }

        } catch (Exception e) {
            System.out.println("Erro atualizar fornecedor = " + e);
        }
        return f;
    }

    public void popularTabela(JTable tabela, String criterio, char situacao) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[3];
        cabecalho[0] = "Código";
        cabecalho[1] = "Nome";
        cabecalho[2] = "CNPJ";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     fornecedor                          "
                    + "WHERE    nome ILIKE '%" + criterio + "%'     "
                    + "AND      situacao = '" + situacao + "'       ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][3];

        } catch (Exception e) {
            System.out.println("Erro ao consultar fornecedor: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   *                                   "
                    + "FROM     fornecedor                          "
                    + "WHERE    nome ILIKE '%" + criterio + "%'     "
                    + "AND      situacao = '" + situacao + "'       "
                    + "ORDER BY nome                                ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = resultadoQ.getString("cnpj");

                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }
}
