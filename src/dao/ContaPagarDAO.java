package dao;

import apoio.ConexaoBD;
import apoio.Formatacao;
import apoio.IDAO_T;
import entidade.ContaPagar;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/*
 * @author fritzzin
 */
public class ContaPagarDAO implements IDAO_T<ContaPagar> {

    ResultSet resultadoQ = null;
    ContaPagar c;

    @Override
    public String salvar(ContaPagar o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO conta_pagar VALUES ("
                    + "DEFAULT, "
                    + "" + o.getFuncionarioId() + "     ,"
                    + " " + o.getValor() + "            ,"
                    + "'" + o.getDataVencimento() + "'  ,"
                    + "'" + o.getDataPagamento() + "'   ,"
                    + "'" + o.getTipoConta() + "'       ,"
                    + "'" + o.getDescricao() + "'       ,"
                    + "'" + o.getMetodoPagamento() + "' ,"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar Contas a Pagar = " + e);
            return e.toString();
        }
    }

    public String salvarSemData(ContaPagar o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO conta_pagar VALUES ("
                    + "DEFAULT, "
                    + " " + o.getFuncionarioId() + "   ,"
                    + " " + o.getValor() + "           ,"
                    + "'" + o.getDataVencimento() + "'  ,"
                    + "     null                        ,"
                    + "'" + o.getTipoConta() + "'       ,"
                    + "'" + o.getDescricao() + "'       ,"
                    + "'" + o.getMetodoPagamento() + "' ,"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar Contas a Pagar = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(ContaPagar o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE conta_pagar \n"
                    + "SET funcionario_id =  " + o.getFuncionarioId() + "    ,  \n"
                    + "valor              =  " + o.getValor() + "            ,  \n"
                    + "data_vencimento    = '" + o.getDataVencimento() + "'  ,  \n"
                    + "tipo_conta         = '" + o.getTipoConta() + "'       ,  \n"
                    + "descricao          = '" + o.getDescricao() + "'       ,  \n"
                    + "metodo_pagamento   = '" + o.getMetodoPagamento() + "' ,  \n"
                    + "situacao           = '" + o.getSituacao() + "'           \n"
                    + "WHERE id = " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar conta a pagar = " + e);
            return e.toString();
        }
    }

    public String atualizarDataPagamento(ContaPagar o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE  conta_pagar \n"
                    + "     SET     data_pagamento  = '" + o.getDataPagamento() + "',\n"
                    + "             situacao       =      'A'\n"
                    + "     WHERE   id              =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar conta a pagar = " + e);
            return e.toString();
        }
    }

    public String removerDataPagamento(ContaPagar o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE  conta_pagar \n"
                    + "     SET     data_pagamento  =      null,\n"
                    + "             situacao        =      'I'  \n"
                    + "     WHERE   id              =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar conta a pagar = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ContaPagar> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ContaPagar> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ContaPagar consultarId(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT * FROM conta_pagar "
                    + "WHERE id = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                c = new ContaPagar();

                int funcionarioId = resultadoQ.getInt("funcionario_id");
                double valor = resultadoQ.getDouble("valor");
                String dataVencimento = Formatacao.ajustaDataDMA(resultadoQ.getString("data_vencimento"));
                String dataPagamento = Formatacao.ajustaDataDMA(resultadoQ.getString("data_pagamento"));
                String tipoConta = resultadoQ.getString("tipo_conta");
                String descricao = resultadoQ.getString("descricao");
                String metodoPagamento = resultadoQ.getString("metodo_pagamento");
                char situacao = resultadoQ.getString("situacao").charAt(0);

                c.setId(id);
                c.setFuncionarioId(funcionarioId);
                c.setValor(valor);
                c.setDataVencimento(dataVencimento);
                c.setDataPagamento(dataPagamento);
                c.setTipoConta(tipoConta);
                c.setDescricao(descricao);
                c.setMetodoPagamento(metodoPagamento);
                c.setSituacao(situacao);
            }

        } catch (Exception e) {
            System.out.println("Erro atualizar conta a pagar = " + e);
        }
        return c;
    }

    public void popularTabela(JTable tabela, char situacao) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[7];
        cabecalho[0] = "Código";
        cabecalho[1] = "Valor";
        cabecalho[2] = "Vencimento";
        cabecalho[3] = "Pagamento";
        cabecalho[4] = "Funcionario";
        cabecalho[5] = "Tipo";
        cabecalho[6] = "Metodo Pagamento";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                     "
                    + "FROM     conta_pagar                  "
                    + "WHERE    situacao = '" + situacao + "'");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][7];

        } catch (Exception e) {
            System.out.println("Erro ao consultar conta_pagar: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   c.id, c.valor, c.data_vencimento, c.data_pagamento,  "
                    + "f.nome AS \"funcionario\",  c.tipo_conta, c.metodo_pagamento  "
                    + "FROM     conta_pagar c, funcionario f        "
                    + "WHERE    c.situacao = '" + situacao + "'     "
                    + "AND      c.funcionario_id = f.id             "
                    + "ORDER BY data_vencimento                     ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = Formatacao.formatarDecimal(resultadoQ.getDouble("valor"));
                dadosTabela[lin][2] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_vencimento"));
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_pagamento"));
                dadosTabela[lin][4] = resultadoQ.getString("funcionario");
                dadosTabela[lin][5] = resultadoQ.getString("tipo_conta");
                dadosTabela[lin][6] = resultadoQ.getString("metodo_pagamento");

                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }

    public void popularTabelaComTipo(JTable tabela, String tipo, char situacao) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[7];
        cabecalho[0] = "Código";
        cabecalho[1] = "Valor";
        cabecalho[2] = "Vencimento";
        cabecalho[3] = "Pagamento";
        cabecalho[4] = "Funcionario";
        cabecalho[5] = "Tipo";
        cabecalho[6] = "Metodo Pagamento";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                        "
                    + "FROM     conta_pagar                     "
                    + "WHERE    situacao    = '" + situacao + "'"
                    + "AND      tipo_conta  = '" + tipo + "'    ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][7];

        } catch (Exception e) {
            System.out.println("Erro ao consultar conta_pagar: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   c.id, c.valor, c.data_vencimento, c.data_pagamento,  "
                    + "f.nome AS \"funcionario\",  c.tipo_conta, c.metodo_pagamento  "
                    + "FROM     conta_pagar c, funcionario f         "
                    + "WHERE    c.situacao       = '" + situacao + "'"
                    + "AND      c.funcionario_id =      f.id         "
                    + "AND      c.tipo_conta     = '" + tipo + "'    "
                    + "ORDER BY data_vencimento                      ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = Formatacao.formatarDecimal(resultadoQ.getDouble("valor"));
                dadosTabela[lin][2] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_vencimento"));
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_pagamento"));
                dadosTabela[lin][4] = resultadoQ.getString("funcionario");
                dadosTabela[lin][5] = resultadoQ.getString("tipo_conta");
                dadosTabela[lin][6] = resultadoQ.getString("metodo_pagamento");

                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }
}
