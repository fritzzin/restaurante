package dao;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.Formatacao;
import apoio.IDAO_T;
import entidade.Comanda;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import tela.comanda.IfrmComanda;

public class ComandaDAO implements IDAO_T<Comanda> {

    ResultSet resultadoQ = null;
    Comanda c;

    @Override
    public String salvar(Comanda o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO comanda VALUES ("
                    + "DEFAULT, "
                    + "" + o.getCliente_id() + ","
                    + "" + o.getFuncionario_id() + ","
                    + "'" + o.getMetodo_pagamento() + "',"
                    + "" + o.getValor() + ","
                    + "'" + o.getData() + "',"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar Comanda = " + e);
            return e.toString();
        }

    }

    @Override
    public String atualizar(Comanda o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "UPDATE comanda "
                    + "SET cliente_id = " + o.getCliente_id() + ", "
                    + "funcionario_id = " + o.getFuncionario_id() + ", "
                    + "metodo_pagamento = '" + o.getMetodo_pagamento() + "', "
                    + "valor = " + o.getValor() + ", "
                    + "data = '" + o.getData() + "', "
                    + "situacao = '" + o.getSituacao() + "' "
                    + "WHERE id = " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro ao atualizar comanda = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "DELETE "
                    + "FROM comanda "
                    + "WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro deletar comanda= " + e);
            return e.toString();
        }
    }

    @Override
    public ArrayList<Comanda> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Comanda> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comanda consultarId(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT * "
                    + "FROM comanda "
                    + "WHERE id = " + id;

            resultadoQ = st.executeQuery(sql);
            System.out.println(sql);

            if (resultadoQ.next()) {
                c = new Comanda();

                int clienteId = resultadoQ.getInt("cliente_id");
                int funcionarioId = resultadoQ.getInt("funcionario_id");
                String metodoPagamento = resultadoQ.getString("metodo_pagamento");
                double valor = resultadoQ.getDouble("valor");
                String data = Formatacao.ajustaDataDMA(resultadoQ.getString("data"));
                char situacao = resultadoQ.getString("situacao").charAt(0);

                c.setId(id);
                c.setCliente_id(clienteId);
                c.setFuncionario_id(funcionarioId);
                c.setMetodo_pagamento(metodoPagamento);
                c.setValor(valor);
                c.setData(data);
                c.setSituacao(situacao);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    public int getId() {
        try {
            int novoId;

            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT max(id) "
                    + "FROM comanda ");

            resultadoQ.next();
            return resultadoQ.getInt("max");
        } catch (SQLException ex) {
            Logger.getLogger(ComandaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public void popularTabela(JTable tabela, char situacao, IfrmComanda janelaPai) {
        int quantidade = 0;
        double valorTotal = 0;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[5];
        cabecalho[0] = "Código";
        cabecalho[1] = "Cliente";
        cabecalho[2] = "Valor";
        cabecalho[3] = "Data";
        cabecalho[4] = "Situação";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][5];

        } catch (Exception e) {
            System.out.println("Erro ao consultar comanda: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT com.id, c.nome AS \"cliente\", com.valor, com.data, com.situacao  "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               "
                    + "ORDER BY com.data                            ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("cliente");
                dadosTabela[lin][2] = resultadoQ.getString("valor");
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data"));
                dadosTabela[lin][4] = resultadoQ.getString("situacao");

                if (resultadoQ.getString("situacao").equals("A")) {
                    dadosTabela[lin][4] = "PAGO";
                } else {
                    dadosTabela[lin][4] = "EM ABERTO";
                }

                quantidade++;
                valorTotal += resultadoQ.getDouble("valor");

                lin++;
            }
            String txtQuantidade = String.valueOf(quantidade);
            String txtValorTotal = String.valueOf(valorTotal);
            janelaPai.setQuantidadeValor(txtQuantidade, txtValorTotal);

        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }

    public void popularTabela(JTable tabela, char situacao, int clienteId, IfrmComanda janelaPai) {
        int quantidade = 0;
        double valorTotal = 0;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[5];
        cabecalho[0] = "Código";
        cabecalho[1] = "Cliente";
        cabecalho[2] = "Valor";
        cabecalho[3] = "Data";
        cabecalho[4] = "Situação";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    com.situacao    = '" + situacao + "'"
                    + "AND      com.cliente_id  =  " + clienteId + ""
                    + "AND      c.id            =  " + clienteId + "");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][5];

        } catch (Exception e) {
            System.out.println("Erro ao consultar comanda: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT com.id, c.nome AS \"cliente\", com.valor, com.data, com.situacao  "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    com.situacao    = '" + situacao + "'"
                    + "AND      com.cliente_id  =  " + clienteId + ""
                    + "AND      c.id            =  " + clienteId + ""
                    + "ORDER BY com.data                            ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("cliente");
                dadosTabela[lin][2] = resultadoQ.getString("valor");
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data"));
                dadosTabela[lin][4] = resultadoQ.getString("situacao");

                if (resultadoQ.getString("situacao").equals("A")) {
                    dadosTabela[lin][4] = "PAGO";
                } else {
                    dadosTabela[lin][4] = "EM ABERTO";
                }

                quantidade++;
                valorTotal += resultadoQ.getDouble("valor");

                lin++;
            }
            String txtQuantidade = String.valueOf(quantidade);
            String txtValorTotal = String.valueOf(valorTotal);
            janelaPai.setQuantidadeValor(txtQuantidade, txtValorTotal);

        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }

    public void popularTabela(JTable tabela, String dataInicio, String dataFim,
            char situacao, IfrmComanda janelaPai) {
        int quantidade = 0;
        double valorTotal = 0;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[5];
        cabecalho[0] = "Código";
        cabecalho[1] = "Cliente";
        cabecalho[2] = "Valor";
        cabecalho[3] = "Data";
        cabecalho[4] = "Situação";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    data BETWEEN '" + dataInicio + "' AND '" + dataFim + "' "
                    + "AND      com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][5];

        } catch (Exception e) {
            System.out.println("Erro ao consultar comanda: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   com.id, c.nome AS \"cliente\", com.valor, com.data, com.situacao  "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    data BETWEEN '" + dataInicio + "' AND '" + dataFim + "' "
                    + "AND      com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               "
                    + "ORDER BY com.data                            "
            );

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("cliente");
                dadosTabela[lin][2] = resultadoQ.getString("valor");
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data"));
                dadosTabela[lin][4] = resultadoQ.getString("situacao");

                if (resultadoQ.getString("situacao").equals("A")) {
                    dadosTabela[lin][4] = "PAGO";
                } else {
                    dadosTabela[lin][4] = "EM ABERTO";
                }

                quantidade++;
                valorTotal += resultadoQ.getDouble("valor");

                lin++;
            }
            String txtQuantidade = String.valueOf(quantidade);
            String txtValorTotal = String.valueOf(valorTotal);
            janelaPai.setQuantidadeValor(txtQuantidade, txtValorTotal);

        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }

    public void popularTabela(JTable tabela, String dataInicio, String dataFim,
            int clienteId, char situacao, IfrmComanda janelaPai) {
        int quantidade = 0;
        double valorTotal = 0;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[5];
        cabecalho[0] = "Código";
        cabecalho[1] = "Cliente";
        cabecalho[2] = "Valor";
        cabecalho[3] = "Data";
        cabecalho[4] = "Situação";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    data BETWEEN '" + dataInicio + "' AND '" + dataFim + "' "
                    + "AND      c.id =          " + clienteId + "   "
                    + "AND      com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][5];

        } catch (Exception e) {
            System.out.println("Erro ao consultar comanda: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   com.id, c.nome AS \"cliente\", com.valor, com.data, com.situacao  "
                    + "FROM     comanda com, cliente c              "
                    + "WHERE    data BETWEEN '" + dataInicio + "' AND '" + dataFim + "' "
                    + "AND      c.id = " + clienteId + "            "
                    + "AND      com.situacao = '" + situacao + "'   "
                    + "AND      com.cliente_id = c.id               "
                    + "ORDER BY com.data                            "
            );

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("cliente");
                dadosTabela[lin][2] = resultadoQ.getString("valor");
                dadosTabela[lin][3] = Formatacao.ajustaDataDMA(resultadoQ.getString("data"));
                dadosTabela[lin][4] = resultadoQ.getString("situacao");

                if (resultadoQ.getString("situacao").equals("A")) {
                    dadosTabela[lin][4] = "PAGO";
                } else {
                    dadosTabela[lin][4] = "EM ABERTO";
                }

                quantidade++;
                valorTotal += resultadoQ.getDouble("valor");

                lin++;
            }
            String txtQuantidade = String.valueOf(quantidade);
            String txtValorTotal = String.valueOf(valorTotal);
            janelaPai.setQuantidadeValor(txtQuantidade, txtValorTotal);

        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }

}
