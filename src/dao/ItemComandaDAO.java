package dao;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.IDAO_T;
import entidade.ItemComanda;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import tela.comanda.DlgComanda;

public class ItemComandaDAO implements IDAO_T<ItemComanda> {

    ResultSet resultadoQ = null;
    ItemComanda item;

    @Override
    public String salvar(ItemComanda o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO item_comanda VALUES ("
                    + "DEFAULT, "
                    + "" + o.getComanda_id() + ","
                    + "" + o.getProduto_id() + ","
                    + "" + o.getQtde() + ","
                    + "" + o.getValor() + ","
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar item_comanda = " + e);
            return e.toString();
        }

    }

    @Override
    public String atualizar(ItemComanda o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE item_comanda "
                    + "SET qtde = '" + o.getQtde() + "' "
                    + "WHERE id = " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar item_comanda = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ItemComanda> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ItemComanda> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ItemComanda consultarId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int qntdItens(int id) {
        int itens = 0;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT COUNT(*) \n"
                    + "FROM item_comanda \n"
                    + "WHERE comanda_id = " + id;

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                itens = resultadoQ.getInt("count");
            }
            return itens;
        } catch (Exception e) {
            e.printStackTrace();
            return itens;
        }
    }

    public int qntdItensEspecifico(int idProduto, int idComanda) {
        int itens = 0;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT qtde                       "
                    + "FROM item_comanda                    "
                    + "WHERE comanda_id = " + idComanda + " "
                    + "AND produto_id = " + idProduto + "   ";

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                itens = resultadoQ.getInt("qtde");
            }
            return itens;
        } catch (Exception e) {
            e.printStackTrace();
            return itens;
        }
    }

    public String excluirTodos(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "DELETE "
                    + "FROM item_comanda "
                    + "WHERE comanda_id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro deletar item_comanda = " + e);
            return e.toString();
        }
    }

    public String excluirItem(int idItem) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "DELETE "
                    + "FROM item_comanda "
                    + "WHERE id = " + idItem;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro deletar item_comanda= " + e);
            return e.toString();
        }
    }

    public void popularTabela(JTable tabela, String criterio, char situacao, DlgComanda comanda) {
        double valorTotal = 0;
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[4];
        cabecalho[0] = "ID";
        cabecalho[1] = "Nome";
        cabecalho[2] = "Quantidade";
        cabecalho[3] = "Valor Unitário";

        // cria matriz de acordo com nº de registros da tabela
        try {

            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     item_comanda                        "
                    + "WHERE    situacao = '" + situacao + "'       ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][4];

        } catch (Exception e) {
            System.out.println("Erro ao consultar item_comanda: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {

            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   ic.id, p.nome AS \"nome\", ic.qtde AS \"qtde\", ic.valor AS \"valor\" "
                    + "FROM     item_comanda ic, produto p          "
                    + "WHERE    ic.comanda_id = '" + criterio + "'  "
                    + "AND      ic.produto_id = p.id                "
                    + "AND      ic.situacao = '" + situacao + "'     "
                    + "ORDER BY ic.id                               ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getString("id");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = resultadoQ.getInt("qtde");
                dadosTabela[lin][3] = resultadoQ.getDouble("valor");

                int qtde = resultadoQ.getInt("qtde");
                double valor = resultadoQ.getDouble("valor");

                valorTotal += (qtde * valor);

                lin++;
            }

            comanda.setValorTotal(valorTotal);

        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class
                    getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }
}
