package dao;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.IDAO_T;
import entidade.Produto;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class ProdutoDAO implements IDAO_T<Produto> {

    ResultSet resultadoQ = null;
    Produto p;

    @Override
    public String salvar(Produto o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO produto VALUES ("
                    + "DEFAULT, "
                    + " " + o.getFornecedor_id() + ","
                    + "'" + o.getNome().toUpperCase() + "',"
                    + "'" + o.getPreco_uni() + "',"
                    + "'" + o.getTipo_produto() + "',"
                    + "'" + o.getDescricao().toUpperCase() + "',"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar produto = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(Produto o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE produto                                     \n"
                    + "SET nome      = '" + o.getNome().toUpperCase() + "' , \n"
                    + "fornecedor_id =  " + o.getFornecedor_id() + "       , \n"
                    + "preco_uni     = '" + o.getPreco_uni() + "'          , \n"
                    + "tipo_produto  = '" + o.getTipo_produto() + "'       , \n"
                    + "descricao     = '" + o.getDescricao() + "'          , \n"
                    + "situacao      = '" + o.getSituacao() + "'             \n"
                    + "WHERE id      =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar produto = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE produto "
                    + "     SET situacao = 'I' "
                    + "     WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro inativar produto = " + e);
            return e.toString();
        }
    }

    public String ativar(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE produto "
                    + "     SET situacao = 'A' "
                    + "     WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro ativar produto = " + e);
            return e.toString();
        }
    }

    @Override
    public ArrayList<Produto> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<Produto> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Produto consultarId(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  SELECT * FROM produto "
                    + "     WHERE id = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                p = new Produto();

                p.setId(id);
                p.setFornecedor_id(resultadoQ.getInt("fornecedor_id"));
                p.setNome(resultadoQ.getString("nome"));
                p.setPreco_uni(resultadoQ.getDouble("preco_uni"));
                p.setTipo_produto(resultadoQ.getString("tipo_produto"));
                p.setDescricao(resultadoQ.getString("descricao"));
                p.setSituacao(resultadoQ.getString("situacao").charAt(0));
            }

        } catch (Exception e) {
            System.out.println("Erro consultar produto = " + e);
        }
        return p;
    }

    public int consultaCriterio(String criterio) {
        int id = 0;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "SELECT id "
                    + "FROM produto "
                    + "WHERE nome = '" + criterio + "'";

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                id = resultadoQ.getInt("id");
            }
        } catch (Exception e) {
            System.out.println("Erro encontrar produto = " + e);
        }

        return id;
    }

    public void popularTabela(JTable tabela, String criterio, char situacao) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[6];
        cabecalho[0] = "Código";
        cabecalho[1] = "Nome";
        cabecalho[2] = "Tipo";
        cabecalho[3] = "Preço";
        cabecalho[4] = "Fornecedor";
        cabecalho[5] = "Descrição";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     produto                             "
                    + "WHERE    nome ILIKE '%" + criterio + "%'     "
                    + "AND      situacao =  '" + situacao + "'       ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][6];

        } catch (Exception e) {
            System.out.println("Erro ao consultar produto: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   p.id, p.nome, p.tipo_produto, p.preco_uni, f.nome AS \"fornecedor\", p.descricao "
                    + "FROM     produto p, fornecedor f                "
                    + "WHERE    p.nome ILIKE '%" + criterio + "%'      "
                    + "AND      p.situacao =  '" + situacao + "'       "
                    + "AND      p.fornecedor_id = f.id                 "
                    + "ORDER BY id                                     ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = resultadoQ.getString("tipo_produto");
                dadosTabela[lin][3] = resultadoQ.getDouble("preco_uni");
                dadosTabela[lin][4] = resultadoQ.getString("fornecedor");
                dadosTabela[lin][5] = resultadoQ.getString("descricao");

                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
    }
}
