package dao;

/*
 * @author fritzzin
 */
import apoio.ConexaoBD;
import apoio.Formatacao;
import apoio.IDAO_T;
import entidade.Funcionario;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import tela.login.DlgLogin;

public class FuncionarioDAO implements IDAO_T<Funcionario> {

    ResultSet resultadoQ = null;
    Funcionario f;

    @Override
    public String salvar(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO funcionario VALUES ("
                    + "DEFAULT, "
                    + o.getFuncao_id() + ","
                    + "'" + o.getNome().toUpperCase() + "',"
                    + "'" + o.getTelefone() + "',"
                    + "'" + o.getData_admissao() + "',"
                    + "'" + o.getData_demissao() + "',"
                    + "'" + o.getSalario() + "',"
                    + "'" + o.getLogin() + "',"
                    + "'" + o.getSenha() + "',"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar funcionario = " + e);
            return e.toString();
        }
    }

    public String salvarSemData(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO funcionario VALUES ("
                    + "DEFAULT, "
                    + o.getFuncao_id() + ","
                    + "'" + o.getNome().toUpperCase() + "',"
                    + "'" + o.getTelefone() + "',"
                    + "'" + o.getData_admissao() + "',"
                    + "null ,"
                    + "'" + o.getSalario() + "',"
                    + "'" + o.getLogin() + "',"
                    + "'" + o.getSenha() + "',"
                    + "'" + o.getSituacao() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro salvar funcionario = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE funcionario \n"
                    + "SET nome             = '" + o.getNome().toUpperCase() + "',  \n"
                    + "funcao_id            =  " + o.getFuncao_id() + ",            \n"
                    + "telefone             = '" + o.getTelefone() + "',            \n"
                    + "data_admissao        = '" + o.getData_admissao() + "',       \n"
                    + "salario              = '" + o.getSalario() + "',             \n"
                    + "login                = '" + o.getLogin() + "',               \n"
                    + "situacao             = '" + o.getSituacao() + "'             \n"
                    + "WHERE id             =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }

    }

    public String atualizarSenha(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE funcionario \n"
                    + "SET senha = '" + o.getSenha() + "' "
                    + "WHERE id  =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }

    }

    public String atualizarDataDemissao(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE funcionario \n"
                    + "SET data_demissao = '" + o.getData_demissao() + "' "
                    + "WHERE id  =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }
    }

    public String removerDataDemissao(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "UPDATE funcionario \n"
                    + "SET data_demissao = null "
                    + "WHERE id  =  " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;

        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE funcionario "
                    + "     SET situacao = 'I' "
                    + "     WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }
    }

    public String ativar(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  UPDATE funcionario "
                    + "     SET situacao = 'A' "
                    + "     WHERE id = " + id;

            int resultado = st.executeUpdate(sql);
            System.out.println(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar funcionario = " + e);
            return e.toString();
        }
    }

    @Override
    public ArrayList<Funcionario> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Funcionario> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Funcionario consultarId(int id) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  SELECT * FROM funcionario "
                    + "     WHERE id = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                f = new Funcionario();

                f.setId(id);
                f.setFuncao_id(resultadoQ.getInt("funcao_id"));
                f.setNome(resultadoQ.getString("nome"));
                f.setTelefone(resultadoQ.getString("telefone"));
                f.setData_admissao(Formatacao.ajustaDataDMA(resultadoQ.getString("data_admissao")));
                f.setData_demissao(Formatacao.ajustaDataDMA(resultadoQ.getString("data_demissao")));
                f.setSalario(resultadoQ.getDouble("salario"));
                f.setLogin(resultadoQ.getString("login"));
                f.setSenha(resultadoQ.getString("senha"));
                f.setSituacao(resultadoQ.getString("situacao").charAt(0));
            }

        } catch (Exception e) {
            System.out.println("Erro consultar funcionario = " + e);
        }
        return f;
    }

    public boolean existeLogin(String loginUsuario) {
        boolean existe = false;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  SELECT  login"
                    + "     FROM    funcionario"
                    + "     WHERE   login = '" + loginUsuario + "'";

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                existe = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            existe = false;
        }

        return existe;
    }

    public boolean login(String loginUsuario, String senhaUsuario, DlgLogin dlgLogin) {
        Funcionario f = new Funcionario();
        boolean acesso = false;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  SELECT  *               \n"
                    + "     FROM    funcionario     \n"
                    + "     WHERE   login = '" + loginUsuario + "'\n"
                    + "     AND     senha = '" + senhaUsuario + "'\n";

//            System.out.println("sql: " + sql);
            resultadoQ = st.executeQuery(sql);

            // Ler campo
            if (resultadoQ.next()) {
                char situacao = resultadoQ.getString("situacao").charAt(0);

                if (situacao == 'A') { // Usuario Ativo
                    acesso = true;

                    int id = resultadoQ.getInt("id");
                    int funcao_id = resultadoQ.getInt("funcao_id");
                    String nome = resultadoQ.getString("nome");
                    String telefone = resultadoQ.getString("telefone");
                    String dataAdmissao = resultadoQ.getString("data_admissao");
                    String dataDemissao = resultadoQ.getString("data_demissao");
                    Double salario = resultadoQ.getDouble("salario");
                    String login = resultadoQ.getString("login");
                    String senha = resultadoQ.getString("senha");

                    f.setId(id);
                    f.setFuncao_id(funcao_id);
                    f.setNome(nome);
                    f.setTelefone(telefone);
                    f.setData_admissao(dataAdmissao);
                    f.setData_demissao(dataDemissao);
                    f.setSalario(salario);
                    f.setLogin(login);
                    f.setSenha(senha);
                    f.setSituacao(situacao);

                } else { // Usuario Inativo
                    JOptionPane.showMessageDialog(null, "Seu usuário está inativo");
                    dlgLogin.inativo = true;
                }
            }

        } catch (Exception e) {
            System.out.println("Erro ao realizar o login = " + e);
        }
        dlgLogin.setFuncionario(f);
        return acesso;
    }

    public void popularTabela(JTable tabela, String criterio, char situacao) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[8];
        cabecalho[0] = "Código";
        cabecalho[1] = "Função";
        cabecalho[2] = "Nome";
        cabecalho[3] = "Telefone";
        cabecalho[4] = "Admissão";
        cabecalho[5] = "Demissão";
        cabecalho[6] = "Salário";
        cabecalho[7] = "Usuário";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   count(*)                            "
                    + "FROM     funcionario                         "
                    + "WHERE    nome ILIKE '%" + criterio + "%'     "
                    + "AND      situacao = '" + situacao + "'       ");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][9];

        } catch (Exception e) {
            System.out.println("Erro ao consultar funcionario: " + e);
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT   f.id, fun.nome AS \"funcao\" , f.nome, f.telefone, "
                    + "f.data_admissao, f.data_demissao, f.salario, f.login "
                    + "FROM     funcionario f, funcao fun                   "
                    + "WHERE    f.nome ILIKE '%" + criterio + "%'   "
                    + "AND      f.situacao = '" + situacao + "'     "
                    + "AND      f.funcao_id = fun.id                "
                    + "ORDER BY f.nome                              ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("funcao");
                dadosTabela[lin][2] = resultadoQ.getString("nome");
                dadosTabela[lin][3] = resultadoQ.getString("telefone");
                dadosTabela[lin][4] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_admissao"));
                dadosTabela[lin][5] = Formatacao.ajustaDataDMA(resultadoQ.getString("data_demissao"));
                dadosTabela[lin][6] = Formatacao.formatarDecimal(resultadoQ.getDouble("salario"));
                dadosTabela[lin][7] = resultadoQ.getString("login");

                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }

    }
}
